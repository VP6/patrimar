const rotas = require('../../config/routes');
const avaliacaoQualidade = require('../controllers/avaliacao_qualidade');

module.exports = function(){

     rotas.get('/avaliacaoQualidade/:id', function(req, res){
        avaliacaoQualidade.get(req.params.id, req, res);
     });
 
     rotas.get('/avaliacaoQualidade', function(req, res){
        avaliacaoQualidade.get(null, req, res);
     });
 
     rotas.post('/avaliacaoQualidade', function(req, res){
        avaliacaoQualidade.post(req.body, req, res);
     });
 
     rotas.put('/avaliacaoQualidade', function(req, res){
        avaliacaoQualidade.put(req.body, req, res);
     });
 
     rotas.delete('/avaliacaoQualidade/:id', function(req, res){
        avaliacaoQualidade.delete(req.params.id, req, res);
     });   
}