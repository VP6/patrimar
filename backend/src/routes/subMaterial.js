const rotas = require('../../config/routes');
const subMaterial = require('../controllers/subMaterial');

module.exports = function(){

     rotas.get('/subMaterial', function(req, res){
        subMaterial.get(req, res);
     });
}