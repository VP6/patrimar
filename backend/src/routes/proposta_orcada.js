const rotas = require('../../config/routes');
const propostaOrcada = require('../controllers/proposta_orcada');

module.exports = function(){

     rotas.get('/propostaOrcada/:id', function(req, res){
        propostaOrcada.get(req.params.id, req, res);
     });
 
     rotas.get('/propostaOrcada', function(req, res){
        propostaOrcada.get(null, req, res);
     });
 
     rotas.post('/propostaOrcada', function(req, res){
        propostaOrcada.post(req.body, req, res);
     });
 
     rotas.put('/propostaOrcada', function(req, res){
        propostaOrcada.put(req.body, req, res);
     });
 
     rotas.delete('/propostaOrcada/:id', function(req, res){
        propostaOrcada.delete(req.params.id, req, res);
     });   
}