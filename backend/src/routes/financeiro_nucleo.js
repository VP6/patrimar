const rotas = require('../../config/routes');
const financeiroNucleo = require('../controllers/financeiro_nucleo');

module.exports = function(){

     rotas.get('/financeiroNucleo/:id', function(req, res){
        financeiroNucleo.get(req.params.id, req, res);
     });
 
     rotas.get('/financeiroNucleo', function(req, res){
        financeiroNucleo.get(null, req, res);
     });
 
     rotas.post('/financeiroNucleo', function(req, res){
        financeiroNucleo.post(req.body, req, res);
     });
 
     rotas.put('/financeiroNucleo', function(req, res){
        financeiroNucleo.put(req.body, req, res);
     });
 
     rotas.delete('/financeiroNucleo/:id', function(req, res){
        financeiroNucleo.delete(req.params.id, req, res);
     });   
}