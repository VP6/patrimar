const rotas = require('../../config/routes');
const alertaJobEmail = require('../controllers/cadastro_email');

module.exports = function(){

     rotas.get('/alertaJobEmail/:id', function(req, res){
        alertaJobEmail.get(req.params.id, req, res);
     });
 
     rotas.get('/alertaJobEmail', function(req, res){
        alertaJobEmail.get(null, req, res);
     });
 
     rotas.post('/alertaJobEmail', function(req, res){
        alertaJobEmail.post(req.body, req, res);
     });
 
     rotas.put('/alertaJobEmail', function(req, res){
        alertaJobEmail.put(req.body, req, res);
     });
 
     rotas.delete('/alertaJobEmail/:id', function(req, res){
        alertaJobEmail.delete(req.params.id, req, res);
     });   
}