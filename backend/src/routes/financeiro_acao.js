const rotas = require('../../config/routes');
const financeiroAcao = require('../controllers/financeiro_acao');

module.exports = function(){

     rotas.get('/financeiroAcao/:id', function(req, res){
        financeiroAcao.get(req.params.id, req, res);
     });
 
     rotas.get('/financeiroAcao', function(req, res){
        financeiroAcao.get(null, req, res);
     });
 
     rotas.post('/financeiroAcao', function(req, res){
        financeiroAcao.post(req.body, req, res);
     });
 
     rotas.put('/financeiroAcao', function(req, res){
        financeiroAcao.put(req.body, req, res);
     });
 
     rotas.delete('/financeiroAcao/:id', function(req, res){
        financeiroAcao.delete(req.params.id, req, res);
     });   
}