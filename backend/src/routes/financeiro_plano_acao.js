const rotas = require('../../config/routes');
const planoFinanceiroAcao = require('../controllers/financeiro_plano_acao');

module.exports = function(){

     rotas.get('/planoFinanceiroAcao/:id', function(req, res){
        planoFinanceiroAcao.get(req.params.id, req, res);
     });
 
     rotas.get('/planoFinanceiroAcao', function(req, res){
        planoFinanceiroAcao.get(null, req, res);
     });
 
     rotas.post('/planoFinanceiroAcao', function(req, res){
        planoFinanceiroAcao.post(req.body, req, res);
     });
 
     rotas.put('/planoFinanceiroAcao', function(req, res){
        planoFinanceiroAcao.put(req.body, req, res);
     });
 
     rotas.delete('/planoFinanceiroAcao/:id', function(req, res){
        planoFinanceiroAcao.delete(req.params.id, req, res);
     });   
}