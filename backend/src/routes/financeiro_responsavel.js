const rotas = require('../../config/routes');
const financeiroResponsavel = require('../controllers/financeiro_responsavel');

module.exports = function(){

     rotas.get('/financeiroResponsavel/:id', function(req, res){
        financeiroResponsavel.get(req.params.id, req, res);
     });
 
     rotas.get('/financeiroResponsavel', function(req, res){
        financeiroResponsavel.get(null, req, res);
     });
 
     rotas.post('/financeiroResponsavel', function(req, res){
        financeiroResponsavel.post(req.body, req, res);
     });
 
     rotas.put('/financeiroResponsavel', function(req, res){
        financeiroResponsavel.put(req.body, req, res);
     });
 
     rotas.delete('/financeiroResponsavel/:id', function(req, res){
        financeiroResponsavel.delete(req.params.id, req, res);
     });   
}