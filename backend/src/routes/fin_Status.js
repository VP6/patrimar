const rotas = require('../../config/routes');
const finStatus = require('../controllers/fin_status');

module.exports = function(){

     rotas.get('/finStatus', function(req, res){
        finStatus.get(req, res);
     });
}