const rotas = require('../../config/routes');
const condicaoPagamento = require('../controllers/condicao_pagamento');

module.exports = function(){

     rotas.get('/condicaoPagamento', function(req, res){
        condicaoPagamento.get(req, res);
     });
}