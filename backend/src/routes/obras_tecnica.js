const rotas = require('../../config/routes');
const obrasTecnica = require('../controllers/obras_tecnica');

module.exports = function(){

     rotas.get('/obrasTecnica/:id', function(req, res){
        obrasTecnica.get(req.params.id, req, res);
     });
 
     rotas.get('/obrasTecnica', function(req, res){
        obrasTecnica.get(null, req, res);
     });
 
     rotas.post('/obrasTecnica', function(req, res){
        obrasTecnica.post(req.body, req, res);
     });
 
     rotas.put('/obrasTecnica', function(req, res){
        obrasTecnica.put(req.body, req, res);
     });
 
     rotas.delete('/obrasTecnica/:id', function(req, res){
        obrasTecnica.delete(req.params.id, req, res);
     });   
}