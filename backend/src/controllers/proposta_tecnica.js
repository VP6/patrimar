const propostaTecnica = require('../models/proposta_tecnica');
const propostaTecnicaItem = require('../models/proposta_tecnica_item');
const resposta = require('../../config/rotinas');


module.exports.get = async function (empreendimento, material, valor, req, res) {
    let retorno = await propostaTecnica.get(empreendimento, material, valor);
    for (let i = 0; i < retorno.result.length; i++) {
        let itens = await propostaTecnicaItem.get(retorno.result[i].CodigoProposta);
        retorno.result[i].Itens = itens.result;

    }
    resposta.montaRetorno(retorno, req, res);
}

module.exports.put = async function (dados, req, res) {
    let itens = dados.Itens;
    dados.Itens = null;
    let retorno = await propostaTecnica.put(dados);
    let retornoDeleteItem = await propostaTecnicaItem.delete(dados.CodigoProposta);

    for (let i = 0; i < itens.length; i++) {
        itens[i].CodigoProposta = dados.CodigoProposta;
        let retornoItem = await propostaTecnicaItem.post(itens[i]);
    }
    let tItens = await propostaTecnicaItem.get(dados.CodigoProposta);
    resposta.montaRetorno(retorno, req, res);
}

module.exports.delete = async function (id, req, res) {
    let retornoDeleteItem = await propostaTecnicaItem.delete(id);
    let retorno = await propostaTecnica.delete(id);
    if (retorno.linhasAfetadas <= 0) {
        retorno.mensage = 'Id não localizado';
    }
    resposta.montaRetorno(retorno, req, res);
}

module.exports.post = async function (dados, req, res) {
    let itens = dados.Itens;
    dados.Itens = null;
    let retorno = await propostaTecnica.post(dados);
    for (let i = 0; i < itens.length; i++) {
        itens[i].CodigoProposta = retorno.result[0].id;
        let retornoItem = await propostaTecnicaItem.post(itens[i]);
    }

    resposta.montaRetorno(retorno, req, res);
}

module.exports.escolherProposta = async function (dados, req, res) {
    let retorno = {};
    for (let i = 0; i < dados.length; i++) {
        propostaTecnica.escolherProposta(dados[i]);
    }
    resposta.montaRetorno(retorno, req, res);
}