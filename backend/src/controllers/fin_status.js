const finStatus = require('../models/fin_Status');
const resposta = require('../../config/rotinas');

module.exports.get = async function(req, res){
    let retorno = await finStatus.get();
    resposta.montaRetorno(retorno, req, res);
}
