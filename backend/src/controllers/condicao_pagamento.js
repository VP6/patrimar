const condicaoPagamento = require('../models/condicao_pagamento');
const resposta = require('../../config/rotinas');

module.exports.get = async function(req, res){
    let retorno = await condicaoPagamento.get();
    resposta.montaRetorno(retorno, req, res);
}
