const subMaterial = require('../models/subMaterial');
const resposta = require('../../config/rotinas');

module.exports.get = async function(req, res){
    let retorno = await subMaterial.get();
    resposta.montaRetorno(retorno, req, res);
}
