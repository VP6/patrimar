const configDB = require('../../config/config_DB');

module.exports.get = async function(id){
    
    let parametro = 'WHERE CodigoPropostaOrcada = ';
    
    // prepara  o parametro
    if(id === null)
        parametro = '';
    else 
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM 
                    VW_PORTAL_PROPOSTA_ORCADA (NOLOCK) 
                    ${parametro}
                  ORDER BY 
                    CodigoObra, 
                    DescricaoMaterial
                    `;
                  
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function(propostaOrcada){
    
    let campos = "";
    let valores = "";

    for (key in propostaOrcada) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof propostaOrcada[key] !== 'object') {
            if (key !== 'CodigoPropostaOrcada'){
                if (campos === ""){
                    campos += key;
                    valores += "'" + propostaOrcada[key] + "'";
                }
                else {
                    campos += "," + key;
                    valores += ",'" + propostaOrcada[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO PropostaOrcada (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id` ;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function(propostaOrcada){
    
    let valores = "";

    for (key in propostaOrcada) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof propostaOrcada[key] !== 'object') {
            if (key !== 'CodigoPropostaOrcada'){
                if (valores === ""){
                    valores += key + " ='" + propostaOrcada[key] + "'";
                }
                else {
                    valores += "," + key + " ='" + propostaOrcada[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE PropostaOrcada SET ${valores}
                        WHERE CodigoPropostaOrcada = ${propostaOrcada.CodigoPropostaOrcada}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function(id){

    //monta a consulta
    let script = `DELETE FROM PropostaOrcada WHERE CodigoPropostaOrcada = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};