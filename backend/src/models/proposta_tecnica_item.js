const configDB = require('../../config/config_DB');

module.exports.get = async function (codigo) {

    //monta a consulta
    let script = `SELECT
                        PropostaTecnica.CodigoProposta
                        ,PropostaTecnicaItem.CodigoMaterialSub
                        ,Material.Descricao
                        ,PropostaTecnicaItem.Peso
                        ,PropostaTecnicaItem.ValorMaterial
                        ,PropostaTecnicaItem.UnidadeMedida
                        ,PropostaTecnicaItem.Quantidade
                        ,PropostaTecnicaItem.DescricaoComplementar
                        ,PropostaTecnicaItem.ValorUnitario
                  FROM
                        PropostaTecnicaItem (NOLOCK)
                        LEFT JOIN Material ON PropostaTecnicaItem.CodigoMaterialSub = Material.CodigoMaterial
                        LEFT JOIN PropostaTecnica ON PropostaTecnica.CodigoProposta = PropostaTecnicaItem.CodigoProposta
                  WHERE
                        (PropostaTecnicaItem.CodigoProposta= ${codigo})`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (proposta) {

    let campos = "";
    let valores = "";

    for (key in proposta) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof proposta[key] !== 'object') {
            if (key !== 'Descricao' ) {
                if (campos === "") {
                    campos += key;
                    valores += "'" + proposta[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + proposta[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO PropostaTecnicaItem (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;

    let resultado = await configDB.executaScriptSQL(script);
    console.log(`Info: ${JSON.stringify(resultado)}`); 
    return resultado;
};

module.exports.delete = async function (id) {
    //monta a consulta
    let script = `DELETE FROM PropostaTecnicaItem WHERE CodigoProposta = ${id} `;
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};