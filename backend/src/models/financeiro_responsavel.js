const configDB = require('../../config/config_DB');

module.exports.get = async function (id) {

    let parametro = 'WHERE CodResponsavel = ';

    // prepara  o parametro
    if (id === null)
        parametro = '';
    else
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM FinResponsavel (NOLOCK) ${parametro}`;



    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (financeiroResponsavel) {

    let campos = "";
    let valores = "";

    for (key in financeiroResponsavel) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroResponsavel[key] !== 'object') {
            if (key !== 'CodResponsavel') {
                if (campos === "") {
                    campos += key;
                    valores += "'" + financeiroResponsavel[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + financeiroResponsavel[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO FinResponsavel (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;

    console.log(script);
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function (financeiroResponsavel) {

    let valores = "";

    for (key in financeiroResponsavel) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroResponsavel[key] !== 'object') {
            if (key !== 'CodResponsavel') {
                if (valores === "") {
                    valores += key + " ='" + financeiroResponsavel[key] + "'";
                } else {
                    valores += "," + key + " ='" + financeiroResponsavel[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE FinResponsavel SET ${valores}
                        WHERE CodResponsavel = ${financeiroResponsavel.CodNucleo}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function (id) {

    //monta a consulta
    let script = `DELETE FROM FinResponsavel WHERE CodResponsavel = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};