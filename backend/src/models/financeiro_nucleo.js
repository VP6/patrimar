const configDB = require('../../config/config_DB');

module.exports.get = async function(id){
    
    let parametro = 'WHERE CodNucleo = ';
    
    // prepara  o parametro
    if(id === null)
        parametro = '';
    else 
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM FinNucleo (NOLOCK) ${parametro}`;
                  
    
   
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function(financeiroNucleo){
    
    let campos = "";
    let valores = "";

    for (key in financeiroNucleo) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroNucleo[key] !== 'object') {
            if (key !== 'CodNucleo'){
                if (campos === ""){
                    campos += key;
                    valores += "'" + financeiroNucleo[key] + "'";
                }
                else {
                    campos += "," + key;
                    valores += ",'" + financeiroNucleo[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO FinNucleo (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id` ;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function(financeiroNucleo){
    
    let valores = "";

    for (key in financeiroNucleo) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroNucleo[key] !== 'object') {
            if (key !== 'CodNucleo'){
                if (valores === ""){
                    valores += key + " ='" + financeiroNucleo[key] + "'";
                }
                else {
                    valores += "," + key + " ='" + financeiroNucleo[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE FinNucleo SET ${valores}
                        WHERE CodNucleo = ${financeiroNucleo.CodNucleo}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function(id){

    //monta a consulta
    let script = `DELETE FROM FinNucleo WHERE CodNucleo = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};