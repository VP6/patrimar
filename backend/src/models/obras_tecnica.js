const configDB = require('../../config/config_DB');

module.exports.get = async function(id){
    
    let parametro = 'WHERE CodigoObra = ';

    // prepara  o parametro
    if(id === null)
        parametro = '';
    else 
        parametro += id;

    //monta a consulta
    let script = `SELECT * 
                FROM ObrasTecnica (NOLOCK) ${parametro}
                ORDER BY Nome`;
                  
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function(obrasTecnica){
    
    let campos = "";
    let valores = "";

    for (key in obrasTecnica) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof obrasTecnica[key] !== 'object') {
            if (key !== 'CodigoObra'){
                if (campos === ""){
                    campos += key;
                    valores += "'" + obrasTecnica[key] + "'";
                }
                else {
                    campos += "," + key;
                    valores += ",'" + obrasTecnica[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO ObrasTecnica (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id` ;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function(obrasTecnica){
    
    let valores = "";
    
    for (key in obrasTecnica) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof obrasTecnica[key] !== 'object') {
            if (key !== 'CodigoObra'){
                if (valores === ""){
                    valores += key + " ='" + obrasTecnica[key] + "'";
                }
                else {
                    valores += "," + key + " ='" + obrasTecnica[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE ObrasTecnica SET ${valores}
                        WHERE CodigoObra = ${obrasTecnica.CodigoObra}`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function(id){

    //monta a consulta
    let script = `DELETE FROM ObrasTecnica WHERE CodigoObra = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};