const configDB = require('../../config/config_DB');

module.exports.get = async function(){
    //monta a consulta
    let script = `SELECT * 
                FROM CondicaoPagamento (NOLOCK) 
                ORDER BY Condicao`;
                  
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};