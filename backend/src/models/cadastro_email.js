const configDB = require('../../config/config_DB');

module.exports.get = async function (id) {

    let parametro = 'WHERE CodigoAlertaJob = ';

    // prepara  o parametro
    if (id === null)
        parametro = '';
    else
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM VW_PORTAL_LISTA_EMAIL (NOLOCK) ${parametro}`;



    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (alertaJobEmail) {

    let campos = "";
    let valores = "";

    for (key in alertaJobEmail) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof alertaJobEmail[key] !== 'object') {
            if (key !== 'CodigoAlertaJob') {
                if (campos === "") {
                    campos += key;
                    valores += "'" + alertaJobEmail[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + alertaJobEmail[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO AlertaJob (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;

    console.log(script);
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function (alertaJobEmail) {

    let valores = "";

    for (key in alertaJobEmail) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof alertaJobEmail[key] !== 'object') {
            if (key !== 'CodigoAlertaJob') {
                if (valores === "") {
                    valores += key + " ='" + alertaJobEmail[key] + "'";
                } else {
                    valores += "," + key + " ='" + alertaJobEmail[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE AlertaJob SET ${valores}
                        WHERE CodigoAlertaJob = ${alertaJobEmail.CodigoAlertaJob}`;
    
    console.log(script);                    
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function (id) {

    //monta a consulta
    let script = `DELETE FROM AlertaJob WHERE CodigoAlertaJob = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};