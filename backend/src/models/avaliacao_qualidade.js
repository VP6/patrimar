const configDB = require('../../config/config_DB');

module.exports.get = async function (id) {

    let parametro = 'WHERE CodigoAvaliacaoComportamento = ';

    // prepara  o parametro
    if (id === null)
        parametro = '';
    else
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM AvaliacaoComportamento (NOLOCK) ${parametro}`;



    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (avaliacaoQualidade) {

    let campos = "";
    let valores = "";

    for (key in avaliacaoQualidade) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof avaliacaoQualidade[key] !== 'object') {
            if (key !== 'CodigoAvaliacaoComportamento') {
                if (campos === "") {
                    campos += key;
                    valores += "'" + avaliacaoQualidade[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + avaliacaoQualidade[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO AvaliacaoComportamento (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;

    console.log(script);
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function (avaliacaoQualidade) {

    let valores = "";

    for (key in avaliacaoQualidade) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof avaliacaoQualidade[key] !== 'object') {
            if (key !== 'CodigoAvaliacaoComportamento') {
                if (valores === "") {
                    valores += key + " ='" + avaliacaoQualidade[key] + "'";
                } else {
                    valores += "," + key + " ='" + avaliacaoQualidade[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE AvaliacaoComportamento SET ${valores}
                        WHERE CodigoAvaliacaoComportamento = ${avaliacaoQualidade.CodigoAvaliacaoComportamento}`;
    
    console.log(script);                    
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function (id) {

    //monta a consulta
    let script = `DELETE FROM AvaliacaoComportamento WHERE CodigoAvaliacaoComportamento = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};