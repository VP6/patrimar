const configDB = require('../../config/config_DB');

module.exports.get = async function(id){
    
    let parametro = 'WHERE CodAcao = ';
    
    // prepara  o parametro
    if(id === null)
        parametro = '';
    else 
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM FinAcao (NOLOCK) ${parametro}`;
                  
    
   
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function(financeiroAcao){
    
    let campos = "";
    let valores = "";

    for (key in financeiroAcao) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroAcao[key] !== 'object') {
            if (key !== 'CodAcao'){
                if (campos === ""){
                    campos += key;
                    valores += "'" + financeiroAcao[key] + "'";
                }
                else {
                    campos += "," + key;
                    valores += ",'" + financeiroAcao[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO FinAcao (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id` ;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function(financeiroAcao){
    
    let valores = "";

    for (key in financeiroAcao) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof financeiroAcao[key] !== 'object') {
            if (key !== 'CodAcao'){
                if (valores === ""){
                    valores += key + " ='" + financeiroAcao[key] + "'";
                }
                else {
                    valores += "," + key + " ='" + financeiroAcao[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE FinAcao SET ${valores}
                        WHERE CodAcao = ${financeiroAcao.CodAcao}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function(id){

    //monta a consulta
    let script = `DELETE FROM FinAcao WHERE CodAcao = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};