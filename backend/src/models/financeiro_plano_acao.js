const configDB = require('../../config/config_DB');

module.exports.get = async function (id) {

    let parametro = 'WHERE CodigoPlanoAcao = ';

    // prepara  o parametro
    if (id === null)
        parametro = '';
    else
        parametro += id;

    //monta a consulta
    let script = `SELECT *
                  FROM VW_FINPLANOACAO (NOLOCK) ${parametro}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (planoFinanceiroAcao) {

    let campos = "";
    let valores = "";

    for (key in planoFinanceiroAcao) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof planoFinanceiroAcao[key] !== 'object') {
            if (key !== 'CodigoPlanoAcao') {
                if (campos === "") {
                    campos += key;
                    valores += "'" + planoFinanceiroAcao[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + planoFinanceiroAcao[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO FinPlanoAcao (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;


    console.log(script);''                    
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function (planoFinanceiroAcao) {

    let valores = "";

    for (key in planoFinanceiroAcao) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof planoFinanceiroAcao[key] !== 'object') {
            if (key !== 'CodigoPlanoAcao') {
                if (valores === "") {
                    valores += key + " ='" + planoFinanceiroAcao[key] + "'";
                } else {
                    valores += "," + key + " ='" + planoFinanceiroAcao[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE FinPlanoAcao SET ${valores}
                        WHERE CodigoPlanoAcao = ${planoFinanceiroAcao.CodigoPlanoAcao}`;

    console.log(script);
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function (id) {

    //monta a consulta
    let script = `DELETE FROM FinPlanoAcao WHERE CodigoPlanoAcao = ${id}`;
    console.log(script);
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};