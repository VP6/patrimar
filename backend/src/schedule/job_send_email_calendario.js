const util = require('../../config/rotinas');
const configJob = require('../../config/config_Job');
const CronJob = require('cron').CronJob;
const nodemailer = require('nodemailer');
const atividadeCalendarioContabil = require('../models/atividade_calendario_contabil');
const alertJob = require('../models/alerta_job');

/*
  * Runs every weekday (Monday through Friday [1-5])
  * at 1:00:00 AM. It does not run on Saturday or Sunday.
  */    
  //cronTime: '00 00 1 * * 1-5',
var job = new CronJob({  
  //cronTime: '*/10 * * * * *', // TESTE DE 10 EM 10 SEG 
  cronTime: '00 00 6 * * 1-5',
  start: false,
  timeZone: 'America/Sao_Paulo',  
  onTick: onExecute
});

//START
job.start();    

console.log('Running job: job_send_email_calendario');

function onExecute() {
    //BUSCA OS DADOS DO CALENDARIO    
    atividadeCalendarioContabil.getCalendario()
    .then(accResult => {                 
        if (accResult && accResult.erro !== null) {            
            job.stop();
            console.log('job_send_email_calendario foi parado devido ao erro: ' + accResult.erro);
        }
        else {            
            //BUSCA OS EMAILS DOS USUÁRIOS CADASTRADOS NO ALERTA
            alertJob.get()
            .then(ajResult => {                
                if (ajResult && ajResult.erro !== null) {    
                    job.stop();
                    console.log('Job: job_send_email_calendario foi parado devido ao erro: ' + ajResult.erro);
                }
                else {    
                    let usuarios = [];
                    usuarios = ajResult.result;                
                    usuarios = usuarios.filter(item => {return item.JobEmailCalendario == "S"});                
                    let emailsTo = [];
                    usuarios.forEach(usuario =>{
                        emailsTo.push(usuario.Email);
                    });
                    
                    if (emailsTo.length > 0) {
                        console.log(emailsTo.join());   
                        const transporter = nodemailer.createTransport(configJob.smtp_config);             
                        const mailOptions = {
                            from: 'no-reply@vp6.com.br',
                            bcc: emailsTo.join(),
                            subject: 'Alerta Calendário',
                            html: util.TemplateHTMLCalendario(accResult.result)                       
                        };
                        
                        console.log('Enviando Email...');                
                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                                job.stop();
                            } else {
                                console.log('Email enviado: ' + info.response);                    
                            }
                        });                                               
                    }
                }
            })
            .catch(error => {
                console.log(error);
                job.stop();
            });
        }        
    })
    .catch(error => {
        console.log(error);
        job.stop();
    });     
}