//objeto padrao de resposta
let resposta = {
    success: false,
    mensage: '',
    result: []
};

//funcao que ja trata o retorna das requisicoes
module.exports.montaRetorno = function(retorno, req, res){
    if (retorno.erro != null){
        resposta.success = false;
        resposta.mensage = retorno.erro;
        resposta.result = [];
        res.status(400).json(resposta);
    }
    else {
        resposta.success = true;
        if ((retorno.mensage != '') && (retorno.mensage != null))
            resposta.mensage = retorno.mensage
        else
            resposta.mensage = '';
        if (retorno.result != null)
            resposta.result = retorno.result;
        else 
            resposta.result = [];
        res.status(200).json(resposta);
    }
};

//formata data para o formato brasileiro
function formataDataBrasileira(data){
    if ((data !== '') && (data != null)){
        let dia = data.substr(8,2);
        let mes = data.substr(5,2);
        let ano = data.substr(0,4);
        return dia + '/' + mes + '/' + ano;
    } 
    else
        return '';
};

function ajustaData(data) {
    //formata o dias
    dia = data.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }
    //formata o mes
    mes = data.getMonth() + 1;
    if (mes < 10) {
        mes = "0" + mes;
    }
    //formata o ano
    ano = data.getFullYear();
    return ano + '-' + mes + '-' + dia;
}

//funcao para gerar o html padrão que será enviado por email.
module.exports.TemplateHTMLCalendario = function(items){
    console.log(items); 
    let html = '';         
    let cabecalhoContabil = '';       
    let dadosContabil = '';       
    let cabecalhoFiscal = '';       
    let dadosFiscal = '';       
    let cabecalhoInterno = '';     
    let dadosInterno = '';     

    let diaSemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
    let hoje = new Date();

    try {
        for (let i = 0; i < 7; i++) {
            let data = new Date();
            data.setDate(hoje.getDate() + i);
            let dataFormatada = ajustaData(data);
    
            //preenche os cabecalhos do calendario
            cabecalhoContabil += 
                `<td style="height: 15px;background-color: #F9ECB6;font-size: 100%;color: #604700;border: 1px solid #ddd; font-family:'Arial'">
                    ${'<center><small><strong>' + diaSemana[data.getDay()] + ' </strong> <br/> ' + formataDataBrasileira(dataFormatada)+'</small></center>'}
                </td>`;
            cabecalhoFiscal += 
                `<td style="height: 15px;background-color: #F9ECB6;font-size: 100%;color: #604700;border: 1px solid #ddd; font-family:'Arial'">
                ${'<center><small><strong>' + diaSemana[data.getDay()] + ' </strong> <br/> ' + formataDataBrasileira(dataFormatada)+'</small></center>'}
                </td>`;
            cabecalhoInterno += 
                `<td style="height: 15px;background-color: #F9ECB6;font-size: 100%;color: #604700;border: 1px solid #ddd; font-family:'Arial'">
                ${'<center><small><strong>' + diaSemana[data.getDay()] + ' </strong> <br/> ' + formataDataBrasileira(dataFormatada)+'</small></center>'}
                </td>`;                
            let descricaoContabil = '',
                descricaoFiscal = '',
                descricaoInterno = '';
    
            //divisão por tipo de calendario
            for (let y = 0; y < items.length; y++) { 
                if (items[y].Data == dataFormatada) {
                    switch (items[y].Tipo) {
                        case 'C':
                            descricaoContabil += `- ${items[y].Descricao}<br>`;
                            break;
                        case 'F':
                            descricaoFiscal += `- ${items[y].Descricao}<br>`;                        
                            break;
                        case 'I':
                            descricaoInterno += `- ${items[y].Descricao}<br>`;
                            break;
                        default:
                            break;
                    }
                }
            }
            //preenche os dados do calendario
            dadosContabil += `<td style="border: 1px solid #ddd;"><small>${descricaoContabil}</small></td>`;
            dadosFiscal += `<td style="border: 1px solid #ddd;"><small>${descricaoFiscal}</small></td>`;        
            dadosInterno += `<td style="border: 1px solid #ddd;"><small>${descricaoInterno}</small></td>`;
        }
    
        html = 
            `
            <style>
        table {
            width: 100%;
            border: 1px solid #337ab7;
            border-radius: 4px;
            table-layout: fixed;
          }
        </style>

            <table style="width: 100%;border: 1px solid #337ab7;border-radius: 4px;" cellspacing="0" cellpadding="0";>        
                <thead>
                    <tr>
                        <td align="center" style="color: #fff;background-color: #337ab7;border-color: #337ab7;">
                            <h4>Calendário Contábil</h4>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <td align="center">
                        <table style="width: 100%;border: 1px solid #ddd;" cellspacing="0" cellpadding="0">        
                            <thead>
                                <tr>
                                    ${cabecalhoContabil}                
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    ${dadosContabil}                
                                </tr>                        
                            </tbody>
                        </table> 
                    </td>                
                </tbody>
            </table>
            <br>
            <br>
            <table style="width: 100%;border: 1px solid #337ab7;border-radius: 4px;" cellspacing="0" cellpadding="0">        
                <thead>
                    <tr>
                        <td align="center" style="color: #fff;background-color: #337ab7;border-color: #337ab7;">
                            <h4>Calendário Fiscal</h4>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <td align="center">
                        <table style="width: 100%;border: 1px solid #ddd;" cellspacing="0" cellpadding="0">        
                            <thead>
                                <tr>
                                    ${cabecalhoFiscal}
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    ${dadosFiscal}                               
                                </tr>
                            </tbody>
                        </table> 
                    </td>                
                </tbody>
            </table>
            <br>
            <br>
            <table style="width: 100%;border: 1px solid #337ab7;border-radius: 4px;" cellspacing="0" cellpadding="0">        
                <thead>
                    <tr>
                        <td align="center" style="color: #fff;background-color: #337ab7;border-color: #337ab7;">
                            <h4>Calendário Interno</h4>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <td align="center">
                        <table style="width: 100%;border: 1px solid #ddd;" cellspacing="0" cellpadding="0">        
                            <thead>
                                <tr>
                                    ${cabecalhoInterno}
                                </tr>    
                            </thead>
                            <tbody>
                                <tr>
                                    ${dadosInterno}                
                                </tr>
                            </tbody>
                        </table> 
                    </td>                
                </tbody>
            </table>`;
        console.log(html);
    } catch (error) {
        html = 'Erro ao gerar o template do HTML.';
    }
   
    return html;

};