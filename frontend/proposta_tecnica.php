<?php
    require_once("layout/cabecalho_layout.php");
?>

<div class="container">
    <!-- Filtro da proposta -->
    <form onsubmit="return listarRegistros()"  id="formFiltro" method="post" >        
        <div class="panel panel-primary" id="filtros">
            <div class="panel-heading">Propostas de Orçamento</div>
            <div class="panel-body">
                <div class="form-group row justify-content-center">
                    <div class="form-group-sm col-sm-12 col-md-3">
                        <label class="control-label" for="selFiltroEmpreendimento">Empreendimento</label>
                        <select id="selFiltroEmpreendimento" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-3">
                        <label class="control-label" for="selFiltroMaterialServico">Material ou Serviço</label>
                        <select id="selFiltroMaterialServico" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-3">
                        <label class="control-label" for="selFiltroValor">Filtrar valor</label>
                        <select id="selFiltroValor" class="form-control">
                            <option value="0"></option>
                            <option value="1">0 a R$500.000,00</option>
                            <option value="2">R$500.000,00 a R$ 1.500.000,00</option>
                            <option value="3">R$1.500.000,00 a R$ 2.000.000,00</option>
                            <option value="4">Acima de R$ 2.000.000,00</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary btn-sm">Pesquisar</button>
            </div>
            <div class="panel-footer">
                <button type="button" class="btn btn-success btn-sm" onclick="novoRegistro()">Adicionar Cotação</button>
            </div>
        </div>
    </form>

    <!-- tabela dos registros -->
    <div class="panel panel-primary" id="lista">
        <div class="panel-heading">Cotações</div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped" id="lista_registros">
                    <thead>
                        <tr>
                            <th>Empreendimento</th>
                            <th>Fornecedor</th>
                            <th>Material/Serviço</th>
                            <th>Valor Total</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody id="lista_corpo">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- formulario de inclusao de propostas -->
    <form onsubmit="return salvarRegistro()" id="formulario" method="post" hidden>
        <div class="panel panel-primary">
            <div class="panel-heading">Cotação</div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="form-group-sm col-sm-12 col-md-4">
                        <input type="text" name="txtCodigo" id="txtCodigo" hidden>
                        <label class="control-label" for="selEmpreendimento">Empreendimento</label>
                        <select id="selEmpreendimento" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-4">
                        <label class="control-label" for="selFornecedor">Fornecedor</label>
                        <select id="selFornecedor" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-4">
                        <label class="control-label" for="txtMaoDeObra">Mão de obra:</label>
                        <input type="text" id="txtMaoDeObra" class="form-control valor" placeholder="Valor mão de obra">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="chbNaoSeAplica" value="option1">
                            <label class="form-check-label" for="chbNaoSeAplica">Não se aplica</label>
                        </div>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-4">
                        <label class="control-label" for="selMaterialServico">Material ou serviço</label>
                        <select id="selMaterialServico" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-4">
                        <label class="control-label" for="selCondicaoPagamento">Condição de Pagamento</label>
                        <select id="selCondicaoPagamento" class="form-control" required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group-sm col-sm-12 col-md-4" 
                            title="CIF – Quando o frete já faz parte do preço.&#013;FOB – Onde o frete é à parte e será informado pelo usuário.">
                        <label class="control-label" for="txtFrete">Frete:</label>
                        <input type="text" id="txtFrete" class="form-control valor" placeholder="Valor frete">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="chbCIFNaoSeAplica" value="option1">
                            <label class="form-check-label" for="chbCIFNaoSeAplica">CIF (Não se aplica)</label>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <caption>
                        <button type="button" class="btn btn-success btn-sm" onclick="modalItem()">Inserir Item</button>
                    </caption>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Material ou serviço</th>
                                <th>Quantidade</th>
                                <th>Unidade</th>
                                <th>Valor Unitário</th>
                                <th>Valor Total</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="lista_item">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                <button type="button" class="btn btn-default btn-sm" onclick="cancelarRegistro()">Cancelar</button>
            </div>
        </div>        
    </form>

    <!-- Modal para edição e inserção de itens -->
    <div class="modal fade" id="modal_item" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #337ab7;color:#FFF">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title">Item complementar</h5>
                </div>
                <div class="modal-body">
                    <form id="form_item">
                        <div class="form-group row">
                            <div class="form-group-sm col-sm-12 col-md-12" id="mensagem_item" hidden>
                                <div class="alert alert-warning" role="alert">
                                    <strong>Atenção!</strong>
                                    <div id="msg_alerta">
                                        <p>Os campos abaixos são obrigatórios:</p>
                                        <ul>
                                            <li>Material ou serviço</li>
                                            <li>Quantidade</li>
                                            <li>Unidade</li>
                                            <li>Valor Unitário</li>                                            
                                            <li>Valor Total</li>                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-sm col-sm-12 col-md-4">
                                <input type="text" id="txtIndiceItem" hidden>
                                <label class="control-label" for="selMaterialServicoSub">Material ou serviço</label>
                                <select id="selMaterialServicoSub" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="form-group-sm col-xs-6 col-sm-12 col-md-2">
                                <label class="control-label" for="txtQuantidade">Quantidade:</label>
                                <input type="number" id="txtQuantidade" class="form-control" placeholder="0" required>
                            </div>
                            <div class="form-group-sm col-xs-6 col-sm-12 col-md-2">
                                <label class="control-label" for="selUnidadeMedida">Unidade:</label>
                                <select id="selUnidadeMedida" class="form-control" placeholder="R$">
                                    <option value=""></option>
                                    <option value="Kg">Kg</option>
                                    <option value="m2">m2</option>
                                    <option value="m3">m3</option>                                    
                                    <option value="Unidade">Unidade (s)</option>                                    
                                </select>
                            </div>
                            <div class="form-group-sm col-xs-6 col-sm-12 col-md-2">
                                <label class="control-label" for="txtValorUnitario">Valor Unitário:</label>
                                <input type="text" id="txtValorUnitario" class="form-control valor" placeholder="R$" required>
                            </div>
                            <div class="form-group-sm col-xs-6 col-sm-12 col-md-2">
                                <label class="control-label" for="txtValorMaterial">Valor Total:</label>
                                <input type="text" id="txtValorMaterial" class="form-control valor" placeholder="R$" required>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="form-group-sm col-sm-12 col-md-12">
                                <label class="control-label" for="txtDescricaoComplementar">Descrição complementar</label>
                                <textarea class="form-control" id="txtDescricaoComplementar" rows="2"></textarea required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" onclick="salvarItem()">Salvar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Formulario modal de confirmacao-->
    <div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title">Confirmar</h5>
                </div>
                <div class="modal-body">
                    <p id="mensagem_modal_confirmar"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btnModalConfirmar">Sim</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/mask.js"></script>
<script src="js/mask_money.js"></script>
<script src="js/config.js"></script>
<script src="js/formularios/proposta_tecnica.js"></script>
<script src="js/toastr.min.js"></script>

<?php
    require_once("layout/rodape_layout.php");
?>