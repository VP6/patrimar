<?php
    require_once("layout/cabecalho_layout.php");
?>

    <div class="container">
        <link rel="stylesheet" href="css/questionario.css">
        <form onsubmit="salvarPerguntas()" id="formulario" method="post">
            <div class="panel panel-primary">
                <div class="panel-heading">Avaliação da Qualidade</div>
                <div class="panel-body">


                    <ul class="nav nav-tabs" id="tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#avaliado" id="avaliado-tab" role="tab" data-toggle="tab" aria-controls="avaliado" aria-expanded="true">Avaliado</a>
                        </li>
                        <li role="presentation">
                            <a href="#identificacao" id="identificacao-tab" role="tab" data-toggle="tab" aria-controls="identificacao" aria-expanded="true">Qualidade da Entrega</a>
                        </li>
                        <li role="presentation">
                            <a href="#satisfacaocliente" id="satisfacaocliente-tab" role="tab" data-toggle="tab" aria-controls="satisfacaocliente" aria-expanded="true">Satisfação do Cliente</a>
                        </li>
                        <li role="presentation">
                            <a href="#demandametas" id="demandametas-tab" role="tab" data-toggle="tab" aria-controls="demandametas" aria-expanded="true">Demanda por Metas</a>
                        </li>
                        <li role="presentation">
                            <a href="#fonteconhecimento" id="fonteconhecimento-tab" role="tab" data-toggle="tab" aria-controls="fonteconhecimento" aria-expanded="true">Fonte de conhecimento</a>
                        </li>
                        <li role="presentation">
                            <a href="#potencialmental" id="potencialmental-tab" role="tab" data-toggle="tab" aria-controls="potencialmental" aria-expanded="true">Potencial mental</a>
                        </li>
                        <li role="presentation">
                            <a href="#motivacao" id="motivacao-tab" role="tab" data-toggle="tab" aria-controls="motivacao" aria-expanded="true">Motivação</a>
                        </li>
                        <li role="presentation">
                            <a href="#modeloseguido" id="modeloseguido-tab" role="tab" data-toggle="tab" aria-controls="modeloseguido" aria-expanded="true">Modelo a ser seguido</a>
                        </li>

                        <li role="presentation">
                        </li>
                        <li role="presentation">
                            <a href="#fazendocerto" id="fazendocerto-tab" role="tab" data-toggle="tab" aria-controls="fazendocerto" aria-expanded="true">Fazendo certo</a>
                        </li>
                        <li role="presentation">
                            <a href="#colaboracaotimes" id="colaboracaotimes-tab" role="tab" data-toggle="tab" aria-controls="colaboracaotimes" aria-expanded="true">Colaboração com outro times</a>
                        </li>
                        <li role="presentation">
                            <a href="#reconhecidotema" id="reconhecidotema-tab" role="tab" data-toggle="tab" aria-controls="reconhecidotema" aria-expanded="true">Reconhecido como referência no tema</a>
                        </li>
                        <li role="presentation">
                            <a href="#questoesabertas" id="questoesabertas-tab" role="tab" data-toggle="tab" aria-controls="questoesabertas" aria-expanded="true">Questões abertas</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="tabs-content">
                        <!-- pergunta 0 -->

                        <div class="tab-pane fade in active" role="tabpanel" id="avaliado" aria-labelledy="avaliado-tab">

                            <input type="text" id="txtCodigo" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Avaliado</h2>
                                </div>
                            </div>
                            <div class=" form-group-sm col-sm-12 col-md-3 ">
                                <label class="control-label" for="selCodigoAvaliado"> Escolha o coloborador para ser avaliado.</label>

                                <label class="control-label" for="selCodigoAvaliado">Nome do avaliado:</label>
                                <select id="selCodigoAvaliado" class="form-control" required>
                                    <option value=""></option>
                                </select>
                            </div>

                            <table class="table table-bordered">
                                <tr>

                                </tr>
                            </table>
                        </div>

                        <!-- pergunta 0 -->

                        <!-- Pergunta UM -->

                        <div class="tab-pane fade " role="tabpanel" id="identificacao" aria-labelledy="identificacao-tab">
                            <input type="text" id="txtCodigo" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Alcançar metas | Clientes</h2>
                                </div>
                            </div>
                            <div class="panel-heading ">
                                <h4 class="title text-center" id="identificacaoModalLabel"> É exemplo de excelência institucional. Garante a entrega das tarefas que estão sob a sua
                                    responsabilidade com assertividade, dentro das melhores práticas contábeis, fiscais e
                                    legais e entrega o resultado comprometido no custo planejado.</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- Cumpre os padrões da área.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Entrega os resultados dentro dos custos e prazos.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">3- Relata as anomalias sempre que identificadas.</th>

                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba1P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba1P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba1P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba1P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba1P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba1P2" value="3" checked="checked" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba1P2" value="2" checked="checked" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba1P2" value="0" checked="checked" class="custom-control-input">
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba1P3" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba1P3" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba1P3" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba1P4" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- Fim pergunta 1  -->

                        <!--  pergunta 2  -->
                        <div class="tab-pane fade" role="tabpanel" id="satisfacaocliente" aria-labelledy="satisfacaocliente-tab">

                            <input type="text" id="txtCodigosatisfacaocliente" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Alcançar metas | Clientes</h2>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel">Sente-se comprometido com a equipe e clientes de forma a garantir as entregas, fortalecendo
                                    uma relação de confiança.</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- Quando demandado por outras áreas, seu foco é entregar as solicitações
                                                        com pontualidade e cordialidade.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Direciona a equipe conforme as orientações do Gestor.
                                                    </th>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba2P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba2P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba2P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba2P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba2P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba2P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba2P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba2P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- Fim Pergunta 2  -->

                        <!-- 3  -->
                        <div class="tab-pane fade" role="tabpanel" id="demandametas" aria-labelledy="demandametas-tab">

                            <input type="text" id="txtCodigodemandametas" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel">Assegura a existência e a qualidade das metas como premissa de todos os projetos institucionais/plano
                                    de 100 dias. Assegura a qualidade das entregas e o cumprimento dos prazos.</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- Atua com foco nas metas.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- contribui de forma proativa na realização das atividades.
                                                    </th>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba3P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba3P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba3P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba3P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba3P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba3P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba3P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba3P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- pergunta 4 -->
                        <div class="tab-pane fade" role="tabpanel" id="fonteconhecimento" aria-labelledy="fonteconhecimento-tab">

                            <input type="text" id="txtCodigofonteconhecimento" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel"> Com o time | Pessoas</h2>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel"> É promotor e mobilizador permanente do desenvolvimento das pessoas e do conhecimento da organização.
                                    Inspira a busca do conhecimento, pesquisa e benchmark ( ele não precisa saber tudo, mas
                                    sim identificar e endereçar os conhecimentos e as necessidades das pessoas e da organização).</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- É demandado pelos colegas para compartilhar sua experiência.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- É proativo na busca de novos conhecimentos.</th>
                                                </tr>



                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba4P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba4P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba4P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba4P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba4P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba4P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba4P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba4P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- pergunta 5 -->
                        <div class="tab-pane fade" role="tabpanel" id="potencialmental" aria-labelledy="potencialmental-tab">

                            <input type="text" id="txtCodigopotencialmental" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel"> É promotor e mobilizador permanente do desenvolvimento das pessoas e do conhecimento da organização.
                                    Inspira a busca do conhecimento, pesquisa e benchmark ( ele não precisa saber tudo, mas
                                    sim identificar e endereçar os conhecimentos e as necessidades das pessoas e da organização).</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- Utiliza ao máximo suas potencial mental para aprender e se autodesenvolver.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Faz uso das boas práticas.</th>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba5P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba5P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba5P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba5P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba5P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba5P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba5P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba5P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- pergunta 6 -->
                        <div class="tab-pane fade" role="tabpanel" id="motivacao" aria-labelledy="motivacao-tab">

                            <input type="text" id="txtCodigomotivacao" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>


                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel"> Tem a consciência dos fundamentos da “ saúde mental” e a atitude correspondente para a promoção
                                    do engajamento das pessoas. Inspira as pessoas e promove a busca pelo alcance das metas
                                    da controladoria.</h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- É comprometido e satisfeito com o trabalho que realiza.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Faz uso adequado dos canais institucionais para tratar de possíveis
                                                        insatisfações.
                                                    </th>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba6P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba6P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba6P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba6P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba6P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba6P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba6P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba6P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </div>
                        <!-- pergunta 7 -->
                        <div class="tab-pane fade" role="tabpanel" id="modeloseguido" aria-labelledy="modeloseguido-tab">

                            <input type="text" id="txtCodigomodeloseguido" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel">É reconhecido, no âmbito de sua área como referencial positivo e de confiança, evidenciado
                                    por suas ações aderentes a melhor governança (walk the talk). Atua como propagador da
                                    cultura.
                                </h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- È demandado pelos colegas para compartilhar sua experiência.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Atua de forma aderente aos padrões de governança.</th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba7P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba7P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba7P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba7P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba7P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba7P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba7P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba7P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- pergunta 8 -->
                        <div class="tab-pane fade" role="tabpanel" id="fazendocerto" aria-labelledy="fazendocerto-tab">

                            <input type="text" id="txtCodigofazendocerto" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <!-- <h4 class="title text-center" id="identificacaoModalLabel">É reconhecido, no âmbito de sua área como referencial positivo e de confiança, evidenciado
                                    por suas ações aderentes a melhor governança (walk the talk). Atua como propagador da
                                    cultura.
                                </h4> -->
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>

                                                <tr scope="row">
                                                    <td scope="row"></td>

                                                    <th scope="col">Sempre</th>


                                    </td>

                                    <th scope="col">Muitas vezes</th>
                                    </td>

                                    <th scope="col">Poucas Vezes</th>
                                    </td>

                                    <th scope="col">Nunca</th>
                                    </td>


                                    </td>
                                    </tr>


                                    <tr>
                                        <th scope="col">1- Meritocracia : Age com isenção e imparcialidade em suas avaliações e feedbacks,
                                            reconhece e recompensa as contribuições individuais e coletivas, focado no crescimento
                                            dos indivíduos e da empresa.</th>
                                        <td scope="row">
                                            <input type="radio" id="5" name="radioAba8P1" value="5" checked="checked" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="3" name="radioAba8P1" value="3" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="2" name="radioAba8P1" value="2" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="0" name="radioAba8P1" value="0" class="custom-control-input">
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="col">2- Obstinação por resultado : Nós somos "Controladoria“. Bater meta é ponta de honra.
                                        </th>
                                        <td scope="row">
                                            <input type="radio" id="5" name="radioAba8P2" value="5" checked="checked" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="3" name="radioAba8P2" value="3" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="2" name="radioAba8P2" value="2" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="0" name="radioAba8P2" value="0" class="custom-control-input">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col">3- transparência e franqueza: verdade com amor. Tem educação e respeito com a equipe
                                            e demais partes envolvidas.</th>
                                        <td scope="row">
                                            <input type="radio" id="5" name="radioAba8P3" value="5" checked="checked" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="3" name="radioAba8P3" value="3" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="2" name="radioAba8P3" value="2" class="custom-control-input">
                                        </td>
                                        <td scope="row">
                                            <input type="radio" id="0" name="radioAba8P3" value="0" class="custom-control-input">
                                        </td>
                                    </tr <tr>
                                    <th scope="col">4- Disciplina: Rigor nos compromissos. Accountability.</th>
                                    <td scope="row">
                                        <input type="radio" id="5" name="radioAba8P4" value="5" checked="checked" class="custom-control-input">
                                    </td>
                                    <td scope="row">
                                        <input type="radio" id="3" name="radioAba8P4" value="3" class="custom-control-input">
                                    </td>
                                    <td scope="row">
                                        <input type="radio" id="2" name="radioAba8P4" value="2" class="custom-control-input">
                                    </td>
                                    <td scope="row">
                                        <input type="radio" id="0" name="radioAba8P4" value="0" class="custom-control-input">
                                    </td>
                                </tr <tr>
                                <th scope="col">5- Atitude de dono: Busca a excelência em tudo e de maneira permanente (não aceita coisa
                                    mal feita), tem senso de urgência e insatisfação constante.</th>
                                <td scope="row">
                                    <input type="radio" id="5" name="radioAba8P5" value="5" checked="checked" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="3" name="radioAba8P5" value="3" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="2" name="radioAba8P5" value="2" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="0" name="radioAba8P5" value="0" class="custom-control-input">
                                </td>
                                </tr <tr>
                                <th scope="col">6- Aprender e ensinar: Sabe que ninguém sabe tudo e que só temos valor como grupo. Utiliza
                                    ao máximo o seu potencial mental e o dos outros. Ensina e está sempre aberto a aprender.</th>
                                <td scope="row">
                                    <input type="radio" id="5" name="radioAba8P6" value="5" checked="checked" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="3" name="radioAba8P6" value="3" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="2" name="radioAba8P6" value="2" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="0" name="radioAba8P6" value="0" value="5" class="custom-control-input">
                                </td>
                                </tr <tr>
                                <th scope="col">7- Bom Humor : Tem prazer no que faz e utiliza o bom humor de forma adequada ao ambiente,
                                    buscando abrir portas e facilitar as relações, seja com a equipe interna ou com os clientes.
                                </th>
                                <td scope="row">
                                    <input type="radio" id="5" name="radioAba8P7" value="5" checked="checked" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="3" name="radioAba8P7" value="3" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="2" name="radioAba8P7" value="2" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="0" name="radioAba8P7" value="0" class="custom-control-input">
                                </td>
                                </tr <tr>
                                <th scope="col">8- Simplicidade: É simples no que faz, prioriza o essencial. Transmite ideias com foco no
                                    entendimento do outro.</th>
                                <td scope="row">
                                    <input type="radio" id="5" name="radioAba8P8" value="5" checked="checked" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="3" name="radioAba8P8" value="3" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="2" name="radioAba8P8" value="2" class="custom-control-input">
                                </td>
                                <td scope="row">
                                    <input type="radio" id="0" name="radioAba8P8" value="0" class="custom-control-input">
                                </td>
                                </tr>
                                </tbody>
                                </table>
                                </td>

                                </tr>
                            </table>

                        </div>

                        <!-- pergunta 9 -->
                        <div class="tab-pane fade" role="tabpanel" id="colaboracaotimes" aria-labelledy="colaboracaotimes-tab">

                            <input type="text" id="txtCodigocolaboracaotimes" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel">Coloca o Interesse da empresa acima de tudo. Atua como Responsável pelo patrimônio intelectual
                                    do grupo Patrimar. Promove acções que conduzem ao aprendizado de todos
                                </h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- identifica boas práticas que possam ser disseminadas.</th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Contribui de forma ativa quando envolvido em ações de melhoria..</th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba9P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba9P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba9P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba9P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba9P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba9P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba9P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba9P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>


                        </div>
                        <!-- pergunta 10 -->

                        <div class="tab-pane fade" role="tabpanel" id="reconhecidotema" aria-labelledy="reconhecidotema-tab">

                            <input type="text" id="txtCodigoreconhecidotema" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Com o time | Pessoas</h2>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <h4 class="title text-center" id="identificacaoModalLabel">É referência em âmbito institucional e da controladoria. entende que um dos papéis da controladoria
                                    dentro do Grupo Patrimar é o de disseminar conhecimento para as demais áreas.
                                </h4>
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba table-bordered">
                                            <tbody>
                                                <td scope="row">
                                                    </br>
                                                </td>

                                                <tr>
                                                    <th scope="col">1- Possui experiência nas técnicas aplicadas a sua área.
                                                    </th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Busca ampliar seu conhecimento com leituras, cursos, palestras e pesquisas.</th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </td>
                                    <td>

                                        <table class="tableaba table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sempre</th>
                                                    <th scope="col">Muitas vezes</th>
                                                    <th scope="col">Poucas Vezes</th>
                                                    <th scope="col">Nunca</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba10P1" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba10P1" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba10P1" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba10P1" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">
                                                        <input type="radio" id="5" name="radioAba10P2" value="5" checked="checked" class="custom-control-input">
                                                    </th>
                                                    <td>
                                                        <input type="radio" id="3" name="radioAba10P2" value="3" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="2" name="radioAba10P2" value="2" class="custom-control-input">
                                                    </td>
                                                    <td>
                                                        <input type="radio" id="0" name="radioAba10P2" value="0" class="custom-control-input">
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </table>


                        </div>
                        <!-- pergunta 11 -->

                        <div class="tab-pane fade" role="tabpanel" id="questoesabertas" aria-labelledy="questoesabertas-tab">

                            <input type="text" id="txtCodigoquestoesabertas" hidden>
                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading abatitulo">
                                    <h2 class="title text-center" id="identificacaoModalLabel">Gerando Conhecimento</h2>
                                </div>
                            </div>

                            <div class="panel-heading">
                                <!-- <h4 class="title text-center" id="identificacaoModalLabel">É referência em âmbito institucional e da controladoria. entende que um dos papéis da controladoria
                                    dentro do Grupo Patrimar é o de disseminar conhecimento para as demais áreas.
                                </h4> -->
                            </div>

                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <table class="tableaba10 table-bordered">
                                            <tbody>


                                                <tr>
                                                    <th scope="col">1- Quais comportamento/Atitudes você identifica no avaliado que ele deveria
                                                        melhorar?.
                                                    </th>
                                                    <th scope="row">
                                                        <span class="caracteres">255</span> Restantes
                                                        <br>
                                                        <textarea rows="4" id=txtArea1 cols="100"></textarea>

                                                    </th>
                                                </tr>

                                                <tr>
                                                    <th scope="col">2- Quais comportamentos/Atitudes você identifica no avaliado que ele
                                                        deveria manter/ potencializar?</th>
                                                    <th scope="row">
                                                        <span class="caracteres1">255</span> Restantes
                                                        <br>
                                                        <textarea rows="4" id=txtArea2 cols="100" maxlength=250></textarea>

                                                    </th>
                                                </tr>


                                                <tr>
                                                    <th scope="col">3- Comentários:</th>
                                                    <th scope="row">
                                                        <span class="caracteres2">255</span> Restantes
                                                        <br>
                                                        <textarea rows="4" id=txtArea3 cols="100" maxlength=250></textarea>

                                                    </th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </td>

                                </tr>
                            </table>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" onclick="exibirConfirmarSemSalvar()">Fechar</button>
                                <button type="submit" class="btn btn-primary">Enviar Resposta</button>
                            </div>
                        </div>


                    </div>

                </div>

                <!-- Formulario modal de confirmacao-->
                <div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h5 class="modal-title">Confirmar</h5>
                            </div>
                            <div class="modal-body">
                                <p id="mensagem_modal_confirmar"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="btnModalConfirmar">Sim</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Formulario modal de confirmacao-->
            <div class="modal fade" id="modal_abertas" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title">Confirmar</h5>
                        </div>
                        <div class="modal-body">
                            <p>Perguntas abertas não preenchidas!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    </div>
    </div>

    <script src="js/mask.js"></script>
    <script src="js/mask_money.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/config.js"></script>
    <script src="js/formularios/avaliacao_qualidade.js"></script>



    <?php
    require_once("layout/rodape_layout.php");
?>