<?php
    require_once("layout/cabecalho_layout.php");
?>

    <div class="container">

        <!-- formulario filtro -->
        <div class="panel panel-primary" id="filtros">
            <div class="panel-heading">Filtros</div>
            <div class="panel-body">
                <form class="form-inline">
                    <label class="control-label" for="txtBuscar">Pesquise na tabela por e-mail:</label>
                    <input type="text" id="txtBuscar" placeholder="Buscar por" class="form-control input-sm">&nbsp;
                </form>
            </div>
        </div>

        <!-- tabela dos registros -->
        <div class="panel panel-primary" id="lista">
            <div class="panel-heading">Listas de Email</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="lista_registros">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Grupo de usuário</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="lista_corpo">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Formulario para manipulacao dos registros -->
        <form onsubmit="salvarRegistro()" id="formulario" method="post" hidden>
            <div class="panel panel-primary">
                <div class="panel-heading">Adicionar Email</div>
                <div class="panel-body">
                    <div class="form-group row">
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <input type="text" name="txtCodigo" id="txtCodigo" hidden>
                            <input type="text" name="txtCodigoUsuario" id="txtCodigoUsuario" hidden>
                            <label class="control-label" for="txtNome">Nome</label>
                            <input type="text" id="txtNome" class="form-control " placeholder="Email" required disabled>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <label class="control-label" for="txtemail">Email</label>
                            <input type="email" id="txtemail" class="form-control " placeholder="Nome" required disabled>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <label class="control-label" for="txtsetor">Grupo de usuário</label>
                            <input type="text" id="txtsetor" class="form-control " placeholder="Setor" required disabled>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="chAlertaEmail" value="N">
                                <label class="form-check-label" for="chAlertaEmail">Receber Email</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    <button type="button" class="btn btn-default btn-sm" onclick="cancelarRegistro()">Cancelar</button>
                </div>
            </div>
        </form>

        <!-- Formulario modal de confirmacao-->
        <div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Confirmar</h5>
                    </div>
                    <div class="modal-body">
                        <p id="mensagem_modal_confirmar"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btnModalConfirmar">Sim</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="js/mask.js"></script>
    <script src="js/mask_money.js"></script>
    <script src="js/config.js"></script>
    <script src="js/formularios/cadastro_email.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>




    <?php
    require_once("layout/rodape_layout.php");
?>