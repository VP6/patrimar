let registros = [];
let registroItens = [];
let subMaterial = [];

function controlaPanel(exibeFormulario) {
    if (exibeFormulario) {
        $('#formulario').show();
        $('#lista').hide();
        $('#filtros').hide();
    } else {
        $('#formulario').hide();
        $('#lista').show();
        $('#filtros').show();
    }
}

function novoRegistro() {    
    controlaPanel(true);
    document.getElementById('formulario').reset();
    registroItens = [];
    preencheItens();
}

function cancelarRegistro() {
    controlaPanel(false);
}

$(window).on("load", function () {
    carregaEmpreendimento();
    carregaFornecedor();
    carregaMaterial();
    carregaSubMaterial();
    carregaCondicaoPagamento();
    
    $("#chbNaoSeAplica").click(function() {
        if($('#chbNaoSeAplica').is(":checked")){
            $('#txtMaoDeObra').val('');
            $('#txtMaoDeObra').attr('readonly', true);
        }
        else
        {
            $('#txtMaoDeObra').attr('readonly', false);
        }
    });
    
    $("#chbCIFNaoSeAplica").click(function() {
        if($('#chbCIFNaoSeAplica').is(":checked")){
            $('#txtFrete').val('');
            $('#txtFrete').attr('readonly', true);
        }
        else
        {
            $('#txtFrete').attr('readonly', false);
        }
    });

    $("#txtQuantidade").keyup(function() {        
        $("#txtValorUnitario").trigger('keyup');    
        $('#txtQuantidade').trigger("focus");
    });

    $("#txtQuantidade").change(function() {        
        $("#txtValorUnitario").trigger('keyup');    
        $('#txtQuantidade').trigger("focus");
    });

    $("#txtValorUnitario").keyup(function() {        
        let quantidade =  $('#txtQuantidade').val();
        let valUnitario = removeMascaraValor(this.value);
        let total = quantidade * valUnitario;
        $('#txtValorMaterial').val(total.toFixed(2).toString().replace('.', ','));                  
        $('#txtValorMaterial').trigger("focus");
        $("#txtValorUnitario").trigger("focus");
    });

    $("#txtValorMaterial").keyup(function() {        
        let quantidade =  $('#txtQuantidade').val();
        let valTotal = removeMascaraValor(this.value);
        let total = valTotal / quantidade;
        $('#txtValorUnitario').val(total.toFixed(2).toString().replace('.', ','));                  
        $('#txtValorUnitario').trigger("focus");
        $('#txtValorMaterial').trigger("focus");
    });

});

function salvarRegistro() {
    let codigo = document.getElementById('txtCodigo').value;
    let empreendimento = document.getElementById('selEmpreendimento').value;
    let fornecedor = document.getElementById('selFornecedor').value;
    let materialServico = document.getElementById('selMaterialServico').value;    
    let valorMaoDeObra = document.getElementById('txtMaoDeObra').value;
    valorMaoDeObra = removeMascaraValor(valorMaoDeObra);
    valorMaoDeObra = valorMaoDeObra.replace('R$ ', '');
    let naoSeAplica;
    if (document.getElementById('chbNaoSeAplica').checked) {
        naoSeAplica = 'S'
    } else {
        naoSeAplica = 'N';
    }
    let condicaoPagamento = document.getElementById('selCondicaoPagamento').value;    
    let valorFrete = document.getElementById('txtFrete').value;
    valorFrete = removeMascaraValor(valorFrete);
    valorFrete = valorFrete.replace('R$ ', '');
    let chbCIFNaoSeAplica;
    if (document.getElementById('chbCIFNaoSeAplica').checked) {
        chbCIFNaoSeAplica = 'S'
    } else {
        chbCIFNaoSeAplica = 'N';
    }

    let registro = {};
    if (codigo != '')
        registro.CodigoProposta = codigo;
    registro.CodigoObra = empreendimento;
    registro.CodigoFornecedor = fornecedor;
    registro.CodigoMaterial = materialServico;
    registro.ValorMaoDeObra = valorMaoDeObra;
    registro.NaoSeAplica = naoSeAplica;
    registro.Escolhida = 'N';
    registro.Validada = 'N';
    registro.Itens = registroItens;
    registro.CodigoCondicaoPagamento = condicaoPagamento;    
    registro.ValorFrete = valorFrete;
    registro.CIFNaoSeAplica = chbCIFNaoSeAplica;
    
    console.log('registroItens', registro);

    toastr.options.positionClass = "toast-top-center";
    toastr.success('Proposta adicionada a base!');
    toastr.options.timeOut = 5000;

    let dados = JSON.stringify(registro);
    console.log(registro.Itens);
    if (codigo == '') {
        gravarBD(dados);

    } else {
        alterarBD(dados);
    }

    return false;
}

function editarRegistro(indice) {
    console.log(registros[indice]);
    controlaPanel(true);
    document.getElementById('txtCodigo').value = registros[indice].CodigoProposta;
    document.getElementById('selEmpreendimento').value = registros[indice].CodigoObra;
    document.getElementById('selFornecedor').value = registros[indice].CodigoFornecedor;
    document.getElementById('selMaterialServico').value = registros[indice].CodigoMaterial;
    document.getElementById('txtMaoDeObra').value = formatValor(registros[indice].ValorMaoDeObra, 'R$ ');
    document.getElementById('selCondicaoPagamento').value = registros[indice].CodigoCondPag;
    document.getElementById('txtFrete').value = formatValor(registros[indice].ValorFrete, 'R$ ');
    registroItens = registros[indice].Itens;
    preencheItens();
    console.log(registroItens);
    if (registros[indice].NaoSeAplica == 'S')
        document.getElementById('chbNaoSeAplica').checked = true;
    else
        document.getElementById('chbNaoSeAplica').checked = false;

    if (registros[indice].CIFNaoSeAplica == 'S')
        document.getElementById('chbCIFNaoSeAplica').checked = true;
    else
        document.getElementById('chbCIFNaoSeAplica').checked = false;
}


function exibirConfirmarExcluir(indice, subMaterial) {
    let mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Atenção! Confirma a exclusão do registro?';
    //captura o button de confirmar do modal_confirmar
    let btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    let onClick = document.createAttribute('onclick');
    //define o evento onclick
    if (subMaterial == true)
        onClick.value = 'excluirItem(' + indice + ')';
    else
        onClick.value = 'excluirRegistro(' + indice + ')';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    $('#modal_confirmar').modal('show');
}

function excluirRegistro(indice) {
    $('#modal_confirmar').modal('hide');
    let codigo = registros[indice].CodigoProposta;
    deletarBD(codigo);
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registros.length - 1) {
        registros.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registros.shift();
    } else {
        let auxInicio = registros.slice(0, indice);
        let auxFim = registros.slice(indice + 1);
        registros = auxInicio.concat(auxFim);
    }
    preencheTable();
}

function salvarItem() {    
    let indiceItem = document.getElementById('txtIndiceItem').value;
    let materialServicoSub = document.getElementById('selMaterialServicoSub').value;
    let subMaterialDescricao = document.getElementById("selMaterialServicoSub").options[document.getElementById("selMaterialServicoSub").selectedIndex].text;    
    let valorMaterial = document.getElementById('txtValorMaterial').value;
    valorMaterial = removeMascaraValor(valorMaterial);
    valorMaterial = valorMaterial.replace('R$ ', '');

    let quantidade = document.getElementById('txtQuantidade').value;
    let unidadeMedida = document.getElementById('selUnidadeMedida').value;
    let valorUnitario = document.getElementById('txtValorUnitario').value;
    valorUnitario = removeMascaraValor(valorUnitario);
    valorUnitario = valorUnitario.replace('R$ ', '');
    let descricaoComplementar = document.getElementById('txtDescricaoComplementar').value;

    //captura o indice do registro pai se estiver em edicao
    let codigoProposta = document.getElementById('txtCodigo').value;  

    //Verifica se os campos obrigatórios foram preenchidos    
    if ((materialServicoSub == 0) || (quantidade == '') || (unidadeMedida == '') || (valorUnitario == '') || (valorMaterial == '') ) {
        document.getElementById('mensagem_item').hidden = false;
    } else {
        let registro = {};
        registro.CodigoMaterialSub = materialServicoSub;
        registro.Descricao = subMaterialDescricao;        
        registro.ValorMaterial = Number(valorMaterial);

        registro.Quantidade = quantidade;
        registro.UnidadeMedida = unidadeMedida;        
        registro.ValorUnitario = Number(valorUnitario);
        registro.DescricaoComplementar = descricaoComplementar;

        if (codigoProposta != '')
            registro.CodigoProposta = codigoProposta;
        
        if (indiceItem != '')
            registroItens[indiceItem] = registro;
        else
            registroItens.push(registro);

        $('#modal_item').modal('hide');
        preencheItens();
    }
}

function editarItem(indice) {
    document.getElementById('mensagem_item').hidden = true;
    document.getElementById('txtIndiceItem').value = indice;
    document.getElementById('selMaterialServicoSub').value = registroItens[indice].CodigoMaterialSub;    
    document.getElementById('txtValorMaterial').value = formatValor(registroItens[indice].ValorMaterial, 'R$ ');

    document.getElementById('txtQuantidade').value = registroItens[indice].Quantidade;
    document.getElementById('selUnidadeMedida').value = registroItens[indice].UnidadeMedida;
    document.getElementById('txtValorUnitario').value = formatValor(registroItens[indice].ValorUnitario, 'R$ ');;
    document.getElementById('txtDescricaoComplementar').value = registroItens[indice].DescricaoComplementar;    

    $('#modal_item').modal('show');
}

function excluirItem(indice) {
    debugger
    $('#modal_confirmar').modal('hide');
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registroItens.length - 1) {
        registroItens.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registroItens.shift();
    } else {
        let auxInicio = registroItens.slice(0, indice);
        let auxFim = registroItens.slice(indice + 1);
        registroItens = auxInicio.concat(auxFim);
    }
    preencheItens();
}

function preencheTable() {
    let tabela = document.getElementById('lista_corpo');
    tabela.innerHTML = '';
    for (let i in registros) {
        tabela.innerHTML +=
            `
            <tr>
                <td>${registros[i].Empreendimento}</td>
                <td>${registros[i].Fornecedor}</td>
                <td>${registros[i].Material}</td>
                <td>${formatValor(registros[i].ValorTotal, 'R$ ')}</td>
                <td>${formataDataBrasileira(registros[i].Data)}</td>
                <td style="white-space: nowrap">
                    <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                    <button class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i})"></button>
                </td>
            </tr>
        `;
    }
}

function preencheItens() {
    let tabela = document.getElementById('lista_item');
    tabela.innerHTML = '';
    for (let i in registroItens) {
        tabela.innerHTML +=`
        <tr>
            <td>${registroItens[i].Descricao}</td>
            <td>${registroItens[i].Quantidade}</td>
            <td>${registroItens[i].UnidadeMedida}</td>
            <td>${formatValor(registroItens[i].ValorUnitario, 'R$ ')}</td>
            <td>${formatValor(registroItens[i].ValorMaterial, 'R$ ')}</td>            
            <td style="white-space: nowrap">
                <button type="button" class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarItem(${i})"></button>&nbsp;
                <button type="button" class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i}, true)"></button>
            </td>
        </tr>
        `
    }
}

function carregaEmpreendimento() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/obrasTecnica');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selEmpreendimento').innerHTML = '';
            document.getElementById('selFiltroEmpreendimento').innerHTML = '';
            $('#selEmpreendimento').append(`<option value=""></option>`);
            $('#selFiltroEmpreendimento').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selEmpreendimento').append(`<option value="${data[x].CodigoObra}">${data[x].Nome}</option>`);
                $('#selFiltroEmpreendimento').append(`<option value="${data[x].CodigoObra}">${data[x].Nome}</option>`);


            }
        }
    }
    xhr.send();
}

function carregaFornecedor() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/fornecedor');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selFornecedor').innerHTML = '';
            $('#selFornecedor').append(`<option value=""></option>`);
            //    $('#selFiltroFornecedor').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selFornecedor').append(`<option value="${data[x].CodigoFornecedor}">${data[x].Nome}</option>`);

            }
        }
    }
    xhr.send();
}

function carregaMaterial() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/material');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selMaterialServico').innerHTML = '';
            document.getElementById('selFiltroMaterialServico').innerHTML = '';
            $('#selMaterialServico').append(`<option value=""></option>`);
            $('#selFiltroMaterialServico').append(`<option value=""></option>`);

            for (let x in data) {
                $('#selMaterialServico').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);
                $('#selFiltroMaterialServico').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);

            }
        }
    }
    xhr.send();
}

function carregaSubMaterial() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/Material');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            subMaterial = data;
            document.getElementById('selMaterialServicoSub').innerHTML = '';
            $('#selMaterialServicoSub').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selMaterialServicoSub').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);

            }
        }
    }
    xhr.send();
}

function carregaCondicaoPagamento() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/condicaoPagamento');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;            
            document.getElementById('selCondicaoPagamento').innerHTML = '';             
            $('#selCondicaoPagamento').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selCondicaoPagamento').append(`<option value="${data[x].Codigo}">${data[x].Condicao}</option>`);

            }
        }
    }
    xhr.send();
}

function modalItem() {
    document.getElementById('form_item').reset();
    document.getElementById('mensagem_item').hidden = true;
    $('#modal_item').modal('show');
    preencheItens();
}

function carregaPropostas(empreendimento, material, valor) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/propostaTecnica/' + empreendimento + '/' + material + '/' + valor);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registros = data;
            if (registros.length == 0) {
                toastr.options.preventDuplicates = true;
                toastr.options.positionClass = "toast-top-center";
                toastr.error('Não há resultados para sua pesquisa');
                toastr.options.timeOut = 5000;
            }
            preencheTable();
        }
    }
    xhr.send();
}

//funcao para gravar um novo registro no bd
function gravarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/propostaTecnica", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
        return data[0].id;
    }
};

//funcao para alterar um registro no bd
function alterarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/propostaTecnica", false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/propostaTecnica/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}

function listarRegistros() {
    let empreendimento = document.getElementById('selFiltroEmpreendimento').value;
    let material = document.getElementById('selFiltroMaterialServico').value;
    let valor = document.getElementById('selFiltroValor').value;
    carregaPropostas(empreendimento, material, valor);
    return false;
}