let registros = [];

function controlaPanel(exibeFormulario) {
    if (exibeFormulario) {
        $('#formulario').show();
        $('#lista').hide();
        $('#filtros').hide();
    } else {
        $('#formulario').hide();
        $('#lista').show();
        $('#filtros').show();
    }
}

function novoRegistro() {
    controlaPanel(true);
    document.getElementById('formulario').reset();
}

function cancelarRegistro() {
    controlaPanel(false);
}
$(window).on("load", function () {
    carregaEmpreendimento();
    carregaMaterial();
    carregaPropostaOrcada();
});

function salvarRegistro() {
    let codigoPropostaOrcada = document.getElementById('txtCodigo').value;
    let empreendimento = document.getElementById('selEmpreendimento').value;
    let materialServico = document.getElementById('selMaterialServico').value;
    let valorMaterial = document.getElementById('txtValorMaterial').value;
    valorMaterial = removeMascaraValor(valorMaterial);
    valorMaterial = valorMaterial.replace('R$ ', '');


    let registro = {};
    if (codigoPropostaOrcada != '')
    registro.CodigoPropostaOrcada = codigoPropostaOrcada;
    registro.CodigoMaterial = materialServico;
    registro.CodigoObra = empreendimento;
    registro.ValorMaterial = valorMaterial;

    toastr.options.positionClass = "toast-top-center";
    toastr.success('Proposta finalizada com sucesso!');
    toastr.options.timeOut = 5000;


    let dados = JSON.stringify(registro);
    if (codigoPropostaOrcada == '')
        gravarBD(dados);
    else
        alterarBD(dados, false);
}

function editarRegistro(indice) {
    controlaPanel(true);
    document.getElementById('txtCodigo').value = registros[indice].CodigoPropostaOrcada;
    document.getElementById('selEmpreendimento').value = registros[indice].CodigoObra;
    document.getElementById('selMaterialServico').value = registros[indice].CodigoMaterial;
    document.getElementById('txtValorMaterial').value = formatValor(registros[indice].ValorMaterial, 'R$ ');
}

function exibirConfirmarExcluir(indice) {
    let mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Atenção! Confirma a exclusão do registro?';
    //captura o button de confirmar do modal_confirmar
    let btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    let onClick = document.createAttribute('onclick');
    //define o evento onclick
    onClick.value = 'excluirRegistro(' + indice + ')';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    
    $('#btnModalConfirmar').show();
    $('#btnModalCancelar').show();
    $('#btnModalOk').hide();
    $('#modal_confirmar .modal-title').html('Confirmar');    

    $('#modal_confirmar').modal('show');
}

function excluirRegistro(indice) {
    $('#modal_confirmar').modal('hide');
    let codigo = registros[indice].CodigoPropostaOrcada;
    deletarBD(codigo);
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registros.length - 1) {
        registros.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registros.shift();
    } else {
        let auxInicio = registros.slice(0, indice);
        let auxFim = registros.slice(indice + 1);
        registros = auxInicio.concat(auxFim);
    }
    preencheTable();
}

function preencheTable() {
    let tabela = document.getElementById('lista_corpo');
    tabela.innerHTML = '';
    for (let i in registros) {
        tabela.innerHTML +=
            `
        <tr>
            <td>
                <input class="form-check-input" type="checkbox" id="ckb_${registros[i].CodigoPropostaOrcada}" data-codigo-proposta-orcada="${registros[i].CodigoPropostaOrcada}">
            </td>
            <td>${registros[i].DescricaoEmprendimento}</td>
            <td>${registros[i].DescricaoMaterial}</td>
            <td>${formatValor(registros[i].ValorMaterial, 'R$ ')}</td>
            <td style="white-space: nowrap">
                <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                <button class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i})"></button>                
            </td>
        </tr>
        `
    }
}

function carregaEmpreendimento() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/obrasTecnica');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selEmpreendimento').innerHTML = '';
            $('#selEmpreendimento').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selEmpreendimento').append(`<option value="${data[x].CodigoObra}">${data[x].Nome}</option>`);

            }
        }
    }
    xhr.send();
}

function carregaMaterial() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/material');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selMaterialServico').innerHTML = '';
            $('#selMaterialServico').append(`<option value=""></option>`);

            for (let x in data) {
                $('#selMaterialServico').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);


            }
        }
    }
    xhr.send();
}
//funcao para carregar os registros do bd
function carregaPropostaOrcada() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/propostaOrcada', false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registros = data;
            preencheTable();
        }
    }
    xhr.send();
}


//funcao para gravar um novo registro no bd
function gravarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/propostaOrcada", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
        return data[0].id;
    }
};

//funcao para alterar um registro no bd
function alterarBD(dados, async) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/propostaOrcada", async);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/propostaOrcada/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}
let registrosNovoValor = [];
function AtualizarRegistros(){
    try {
        $('#txtValorAtualizacao').val('');
        registrosNovoValor = [];
        $('#tbOrcamentosAtualizacao').html('');
        let table = document.getElementById('lista_registros');
        let rowCount = table.rows.length;
        let hasChecked = false;
        //i = 0 header
        for (var i = 1; i < rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if (null != chkbox && true == chkbox.nextElementSibling.checked) {                
                hasChecked = true;                
                registrosNovoValor.push(
                    {
                        CodigoPropostaOrcada: chkbox.nextElementSibling.dataset.codigoPropostaOrcada,
                        ValorMaterial: registros[i-1].ValorMaterial
                    }
                );

                $('#tbOrcamentosAtualizacao').append(`
                    <tr>
                        <td>${row.cells[1].innerText}</td>
                        <td>${row.cells[2].innerText}</td>
                        <td><strong>${row.cells[3].innerText}</strong></td>
                    </tr>`
                );
            }
        }
        if (hasChecked) {
            $('#modal_atualizar').modal('show');
        }
        else {
            MsgAlert('Selecione pelo menos um item da lista.', false)
        }        
    } catch (e) {
      console.log(e);
    }
}

function AtualizarValor()
{   
    toastr.options.positionClass = "toast-top-center";
    toastr.success('Atualizando valores. Aguarde...');
    toastr.options.timeOut = 5000;
    
    setTimeout(()=>{
        SalvarNovoValor();
    }, 5000);
}

function SalvarNovoValor()
{   
    debugger

    if (registrosNovoValor.length > 0) {        
        let valorIndexAtualizacao = $('#txtValorAtualizacao').val();
        if (valorIndexAtualizacao && valorIndexAtualizacao != "") {                                        
            for (let index in registrosNovoValor) {            
                let valorMaterial = registrosNovoValor[index].ValorMaterial
                registrosNovoValor[index].ValorMaterial = (valorMaterial * valorIndexAtualizacao);
                let dados = JSON.stringify(registrosNovoValor[index]);
                console.log(dados);
                alterarBD(dados, false);
            }            

            Refresh();
        }  
        else
        {
            $('#txtValorAtualizacao').focus();
        }        
    }    
}

function Refresh() {
    $('#modal_atualizar').modal('hide');
    $('#ckbSelecionar').prop('checked', false);
    carregaPropostaOrcada();
    $('#txtBuscar').focus();
}

function MsgAlert(msg) {
    $('#mensagem_modal_confirmar').html(msg);
    $('#btnModalConfirmar').hide();
    $('#btnModalCancelar').hide();
    $('#btnModalOk').show();
    $('#modal_confirmar .modal-title').html('Atenção');
    $('#modal_confirmar').modal('show');
}
