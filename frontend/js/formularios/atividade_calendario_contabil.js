let registros = [];

function controlaPanel(exibeFormulario) {
    if (exibeFormulario) {
        $('#formulario').show();
        $('#lista').hide();
        $('#filtros').hide();
    } else {
        $('#formulario').hide();
        $('#lista').show();
        $('#filtros').show();
    }
}

function novoRegistro() {
    controlaPanel(true);
    document.getElementById('formulario').reset();
}

function cancelarRegistro() {
    controlaPanel(false);
}


$(window).on("load", function () {
    carregaAtividadeCalendarioContabil();
});

function salvarRegistro() {
    let codigo = document.getElementById('txtCodigo').value;
    let descricao = document.getElementById('txtDescricao').value;
    let tipo = document.getElementById('selTipo').value;
    let periodicidade = document.getElementById('selPeriodicidade').value;
    let data = formataData(document.getElementById('txtData').value);
    let dataFim = formataData(document.getElementById('txtDataFim').value);



    let recorrente = moment(dataFim).diff(moment(data), 'months', true);
    let recorrenteDia = moment(dataFim).diff(moment(data), 'days', true);


    let dataFds = GetWeekday(data);
    let dataFdsFim = GetWeekday(dataFim);



    let registro = {};
    if (codigo != '') {
        registro.CodigoAtividade = codigo;
    } else {
        //captura o codigo do usuario
        var cookie = $.parseJSON($.cookie('VP6_Patrimar_Session'));
        registro.InseridoPor = cookie.CodigoUsuario;
    }
    registro.Descricao = descricao;
    registro.Data = dataFds;
    registro.DataFim = dataFdsFim;
    registro.Periodicidade = periodicidade;
    registro.Tipo = tipo;


    let dados = JSON.stringify(registro);


    if (codigo == '') {
        if (recorrente < 1) {
            for (let index = 0; index < recorrenteDia; index++) {
                if (index == 0) {

                    gravarBD(JSON.stringify(registro));
                }
                let diaRef = moment(registro.Data).add(1, 'day').format('YYYY-MM-DD');
                registro.Data = diaRef;
                gravarBD(JSON.stringify(registro));


            }
        } else
            for (let index = 0; index < recorrente; index++) {
                if (index == 0) {

                    gravarBD(JSON.stringify(registro));
                }
                let mesRef = moment(registro.Data).add(1, 'month').format('YYYY-MM-DD');
                registro.Data = GetWeekday(mesRef);
                gravarBD(JSON.stringify(registro));

            }
    } else
        alterarBD(JSON.stringify(registro)) ;

}


function editarRegistro(indice) {
    controlaPanel(true);
    document.getElementById('txtCodigo').value = registros[indice].CodigoAtividade;
    document.getElementById('txtDescricao').value = registros[indice].Descricao;
    document.getElementById('selTipo').value = registros[indice].Tipo;
    document.getElementById('selPeriodicidade').value = registros[indice].Periodicidade;
    document.getElementById('txtData').value = formataDataBrasileira(registros[indice].Data);
    document.getElementById('txtDataFim').value = formataDataBrasileira(registros[indice].DataFim);
}

function exibirConfirmarExcluir(indice) {
    let mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Atenção! Confirma a exclusão do registro?';
    //captura o button de confirmar do modal_confirmar
    let btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    let onClick = document.createAttribute('onclick');
    //define o evento onclick
    onClick.value = 'excluirRegistro(' + indice + ')';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    $('#modal_confirmar').modal('show');
}

function excluirRegistro(indice) {
    $('#modal_confirmar').modal('hide');
    let codigo = registros[indice].CodigoAtividade;
    deletarBD(codigo);
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registros.length - 1) {
        registros.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registros.shift();
    } else {
        let auxInicio = registros.slice(0, indice);
        let auxFim = registros.slice(indice + 1);
        registros = auxInicio.concat(auxFim);
    }
    preencheTable();
}

function preencheTable() {
    let tabela = document.getElementById('lista_corpo');
    tabela.innerHTML = '';
    for (let i in registros) {
        tabela.innerHTML +=
            `
        <tr>
            <td>${registros[i].Descricao}</td>
            <td>${registros[i].DescricaoTipo}</td>
            <td>${registros[i].DescricaoPeriodicidade}</td>
            <td>${formataDataBrasileira(registros[i].Data)}</td>
            <td>${formataDataBrasileira(registros[i].DataFim)}</td>
            <td style="white-space: nowrap">
                <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                <button class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i})"></button>
            </td>
        </tr>
        `
    }
}

//funcao para gravar um novo registro no bd
function gravarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/atividadeCalendarioContabil", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
        return data[0].id;
    }
};

//funcao para alterar um registro no bd
function alterarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/atividadeCalendarioContabil", false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/atividadeCalendarioContabil/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}

//funcao para carregar os registros do bd
function carregaAtividadeCalendarioContabil() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/atividadeCalendarioContabil', false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registros = data;
            preencheTable();
        }
    }
    xhr.send();
}

function GetWeekday(date) {
    debugger
    var dt = new Date(date);

    if ((dt.getDay() === 5) || (dt.getDay() === 6)) {
        //sabado
        if (dt.getDay() === 5) {
            date = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 3);
        }
        //domingo
        if (dt.getDay() === 6) {
            date = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 2);
        }
    }
    return date;
}