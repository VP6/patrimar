let registros = [];
let registroItens = [];
let registroItensProposta = [];


function controlaPanel(exibeFormulario) {
    if (exibeFormulario) {
        $('#formulario').show();
        $('#lista').hide();
        $('#filtros').hide();
    } else {
        $('#formulario').hide();
        $('#lista').show();
        $('#filtros').show();
    }
}

function cancelarRegistro() {
    controlaPanel(false);
}

$(window).on("load", function () {

    carregaEmpreendimento();
    carregaMaterial();
    carregaFornecedor();
    carregaSubMaterial();    
    carregaCondicaoPagamento();

    $("#chbNaoSeAplica").click(function() {
        if($('#chbNaoSeAplica').is(":checked")){
            $('#txtMaoDeObra').val('');
            $('#txtMaoDeObra').attr('readonly', true);
        }
        else
        {
            $('#txtMaoDeObra').attr('readonly', false);
        }
    });

    $("#chbNaoSeAplica").click(function() {
        if($('#chbNaoSeAplica').is(":checked")){
            $('#txtMaoDeObra').val('');
            $('#txtMaoDeObra').attr('readonly', true);
        }
        else
        {
            $('#txtMaoDeObra').attr('readonly', false);
        }
    });
    
    $("#chbCIFNaoSeAplica").click(function() {
        if($('#chbCIFNaoSeAplica').is(":checked")){
            $('#txtFrete').val('');
            $('#txtFrete').attr('readonly', true);
        }
        else
        {
            $('#txtFrete').attr('readonly', false);
        }
    });

    $("#txtQuantidade").keyup(function() {        
        $("#txtValorUnitario").trigger('keyup');    
        $('#txtQuantidade').trigger("focus");
    });

    $("#txtQuantidade").change(function() {        
        $("#txtValorUnitario").trigger('keyup');    
        $('#txtQuantidade').trigger("focus");
    });

    $("#txtValorUnitario").keyup(function() {        
        let quantidade =  $('#txtQuantidade').val();
        let valUnitario = removeMascaraValor(this.value);
        let total = quantidade * valUnitario;
        $('#txtValorMaterial').val(total.toFixed(2).toString().replace('.', ','));                  
        $('#txtValorMaterial').trigger("focus");
        $("#txtValorUnitario").trigger("focus");
    });

    $("#txtValorMaterial").keyup(function() {        
        let quantidade =  $('#txtQuantidade').val();
        let valTotal = removeMascaraValor(this.value);
        let total = valTotal / quantidade;
        $('#txtValorUnitario').val(total.toFixed(2).toString().replace('.', ','));                  
        $('#txtValorUnitario').trigger("focus");
        $('#txtValorMaterial').trigger("focus");
    });
});

function preencheTable() {

    let menorPrecoMaterial = document.getElementById('lblFornecedorMenorPrecoMaterial');
    let menorPrecoMaoDeObra = document.getElementById('lblFornecedorMenorPrecoMaoDeObra');
    let menorPrecoTotal = document.getElementById('lblFornecedorMenorPrecoTotal');
    let tabela = document.getElementById('lista_corpo');    
    tabela.innerHTML = '';

    let menorTotal = 0;
    let menorMaterial = 0;
    let menorMaoDeObra = 0;

   $('#lblEmpreendimento').html('');
   $('#lblMaterialServico').html('');

    for (let i in registros) {

        $('#lblEmpreendimento').html($('#selFiltroEmpreendimento option:selected').text());
        $('#lblMaterialServico').html($('#selFiltroMaterialServico option:selected').text());

        tabela.innerHTML +=
            `
            <tr>                
                <td>${registros[i].Fornecedor}</td>                
                <td>${formatValor(registros[i].ValorMaterial, 'R$ ')}</td>                
                <td>${formatValor(registros[i].ValorMaoDeObra, 'R$ ')}</td>                
                <td>${formatValor(registros[i].ValorFrete, 'R$ ')}</td>          
                <td>${registros[i].DescCondPag}</td>                                      
                <td>${formatValor(registros[i].ValorTotal, 'R$ ')}</td>                
                <td>${formataDataBrasileira(registros[i].Data)}</td>
                <td style="white-space: nowrap">
                    <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                    <button class="btn btn-danger btn-xs selecao" title="Escolher Proposta" id="btn${i}" onclick="escolherProposta(${i})">Escolher Proposta</button>&nbsp;
                </td>
            </tr>
        `;
    }

    if (registros.length > 0) {
        var objMinValorTotal = registros.reduce(function(res, obj) {
            return (obj.ValorTotal < res.ValorTotal) ? obj : res;
        });
        menorPrecoTotal.innerHTML = `
                <div class="col col-sm-2">Menor Preço <strong>Total:</strong></div>
                <div class="col col-sm-4">${objMinValorTotal.Fornecedor}</div>
                <div class="col col-sm-4"><strong>${formatValor(objMinValorTotal.ValorTotal, 'R$ ')}</strong></div>`;
    
        var objMinValorMaterial = registros.reduce(function(res, obj) {
            return (obj.ValorMaterial < res.ValorMaterial) ? obj : res;
        });
        menorPrecoMaterial.innerHTML = `                
                <div class="col col-sm-2">Menor Preço <strong>Material:</strong></div>
                <div class="col col-sm-4">${objMinValorMaterial.Fornecedor}</div>
                <div class="col col-sm-4"><strong>${formatValor(objMinValorMaterial.ValorMaterial, 'R$ ')}</strong></div>`;  
        var objMinValorMaoDeObra = registros.reduce(function(res, obj) {
            return (obj.ValorMaoDeObra < res.ValorMaoDeObra) ? obj : res;
        });
        menorPrecoMaoDeObra.innerHTML = `                
                <div class="col col-sm-2">Menor Preço <strong>Mão de Obra:</strong></div>
                <div class="col col-sm-4">${objMinValorMaoDeObra.Fornecedor}</div>
                <div class="col col-sm-4"><strong>${formatValor(objMinValorMaoDeObra.ValorMaoDeObra, 'R$ ')}</strong></div>`;    
    }
}

function preencheItens() {
    let tabela = document.getElementById('lista_item');
    tabela.innerHTML = '';
    for (let i in registroItens) {
        tabela.innerHTML +=`
        <tr>
            <td>${registroItens[i].Descricao}</td>
            <td>${registroItens[i].Quantidade}</td>
            <td>${registroItens[i].UnidadeMedida}</td>
            <td>${formatValor(registroItens[i].ValorUnitario, 'R$ ')}</td>
            <td>${formatValor(registroItens[i].ValorMaterial, 'R$ ')}</td>            
            <td style="white-space: nowrap">
                <button type="button" class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarItem(${i})"></button>&nbsp;
                <button type="button" class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i}, true)"></button>
            </td>
        </tr>`
    }
}

function cancelarRegistro() {
    controlaPanel(false);
}

function salvarRegistro() {
    let codigo = document.getElementById('txtCodigo').value;
    let empreendimento = document.getElementById('selEmpreendimento').value;
    let fornecedor = document.getElementById('selFornecedor').value;
    let materialServico = document.getElementById('selMaterialServico').value;    
    let valorMaoDeObra = document.getElementById('txtMaoDeObra').value;
    valorMaoDeObra = removeMascaraValor(valorMaoDeObra);
    valorMaoDeObra = valorMaoDeObra.replace('R$ ', '');
    let naoSeAplica;
    if (document.getElementById('chbNaoSeAplica').checked) {
        naoSeAplica = 'S'
    } else {
        naoSeAplica = 'N';
    }
    let condicaoPagamento = document.getElementById('selCondicaoPagamento').value;    
    let valorFrete = document.getElementById('txtFrete').value;
    valorFrete = removeMascaraValor(valorFrete);
    valorFrete = valorFrete.replace('R$ ', '');
    let chbCIFNaoSeAplica;
    if (document.getElementById('chbCIFNaoSeAplica').checked) {
        chbCIFNaoSeAplica = 'S'
    } else {
        chbCIFNaoSeAplica = 'N';
    }

    let registro = {};
    if (codigo != '')
        registro.CodigoProposta = codigo;
    registro.CodigoObra = empreendimento;
    registro.CodigoFornecedor = fornecedor;
    registro.CodigoMaterial = materialServico;
    registro.ValorMaoDeObra = valorMaoDeObra;
    registro.NaoSeAplica = naoSeAplica;
    registro.Escolhida = 'N';
    registro.Validada = 'N';
    registro.Itens = registroItens;
    registro.CodigoCondicaoPagamento = condicaoPagamento;    
    registro.ValorFrete = valorFrete;
    registro.CIFNaoSeAplica = chbCIFNaoSeAplica;
    
    console.log('registroItens', registro);

    toastr.options.positionClass = "toast-top-center";
    toastr.success('Proposta adicionada a base!');
    toastr.options.timeOut = 5000;

    let dados = JSON.stringify(registro);
    console.log(registro.Itens);
    if (codigo == '') {
        gravarBD(dados);

    } else {
        alterarBD(dados);
    }

    return false;
}

function editarRegistro(indice) {
    controlaPanel(true);
    document.getElementById('txtCodigo').value = registros[indice].CodigoProposta;
    document.getElementById('selEmpreendimento').value = registros[indice].CodigoObra;
    document.getElementById('selFornecedor').value = registros[indice].CodigoFornecedor;
    document.getElementById('selMaterialServico').value = registros[indice].CodigoMaterial;
    document.getElementById('txtMaoDeObra').value = formatValor(registros[indice].ValorMaoDeObra, 'R$ ');
    document.getElementById('selCondicaoPagamento').value = registros[indice].CodigoCondPag;
    document.getElementById('txtFrete').value = formatValor(registros[indice].ValorFrete, 'R$ ');
    registroItens = registros[indice].Itens;
    preencheItens();
    console.log(registroItens);
    if (registros[indice].NaoSeAplica == 'S')
        document.getElementById('chbNaoSeAplica').checked = true;
    else
        document.getElementById('chbNaoSeAplica').checked = false;

    if (registros[indice].CIFNaoSeAplica == 'S')
        document.getElementById('chbCIFNaoSeAplica').checked = true;
    else
        document.getElementById('chbCIFNaoSeAplica').checked = false;
}

function exibirConfirmarExcluir(indice, subMaterial) {
    let mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Atenção! Confirma a exclusão do registro?';
    //captura o button de confirmar do modal_confirmar
    let btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    let onClick = document.createAttribute('onclick');
    //define o evento onclick
    if (subMaterial == true)
        onClick.value = 'excluirItem(' + indice + ')';
    else
        onClick.value = 'excluirRegistro(' + indice + ')';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    $('#modal_confirmar').modal('show');
}

function excluirRegistro(indice) {
    $('#modal_confirmar').modal('hide');
    let codigo = registros[indice].CodigoProposta;
    deletarBD(codigo);
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registros.length - 1) {
        registros.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registros.shift();
    } else {
        let auxInicio = registros.slice(0, indice);
        let auxFim = registros.slice(indice + 1);
        registros = auxInicio.concat(auxFim);
    }
    preencheTable();
}

function salvarItem() {
    let indiceItem = document.getElementById('txtIndiceItem').value;
    let materialServicoSub = document.getElementById('selMaterialServicoSub').value;
    let subMaterialDescricao = document.getElementById("selMaterialServicoSub").options[document.getElementById("selMaterialServicoSub").selectedIndex].text;    
    let valorMaterial = document.getElementById('txtValorMaterial').value;
    valorMaterial = removeMascaraValor(valorMaterial);
    valorMaterial = valorMaterial.replace('R$ ', '');

    let quantidade = document.getElementById('txtQuantidade').value;
    let unidadeMedida = document.getElementById('selUnidadeMedida').value;
    let valorUnitario = document.getElementById('txtValorUnitario').value;
    valorUnitario = removeMascaraValor(valorUnitario);
    valorUnitario = valorUnitario.replace('R$ ', '');
    let descricaoComplementar = document.getElementById('txtDescricaoComplementar').value;

    //captura o indice do registro pai se estiver em edicao
    let codigoProposta = document.getElementById('txtCodigo').value;  

    //Verifica se os campos obrigatórios foram preenchidos    
    if ((materialServicoSub == 0) || (quantidade == '') || (unidadeMedida == '') || (valorUnitario == '') || (valorMaterial == '') ) {
        document.getElementById('mensagem_item').hidden = false;
    } else {
        let registro = {};
        registro.CodigoMaterialSub = materialServicoSub;
        registro.Descricao = subMaterialDescricao;        
        registro.ValorMaterial = Number(valorMaterial);

        registro.Quantidade = quantidade;
        registro.UnidadeMedida = unidadeMedida;        
        registro.ValorUnitario = Number(valorUnitario);
        registro.DescricaoComplementar = descricaoComplementar;

        if (codigoProposta != '')
            registro.CodigoProposta = codigoProposta;
        
        if (indiceItem != '')
            registroItens[indiceItem] = registro;
        else
            registroItens.push(registro);

        $('#modal_item').modal('hide');
        preencheItens();
    }
}

function editarItem(indice) {
    document.getElementById('mensagem_item').hidden = true;
    document.getElementById('txtIndiceItem').value = indice;
    document.getElementById('selMaterialServicoSub').value = registroItens[indice].CodigoMaterialSub;    
    document.getElementById('txtValorMaterial').value = formatValor(registroItens[indice].ValorMaterial, 'R$ ');

    document.getElementById('txtQuantidade').value = registroItens[indice].Quantidade;
    document.getElementById('selUnidadeMedida').value = registroItens[indice].UnidadeMedida;
    document.getElementById('txtValorUnitario').value = formatValor(registroItens[indice].ValorUnitario, 'R$ ');;
    document.getElementById('txtDescricaoComplementar').value = registroItens[indice].DescricaoComplementar;    

    $('#modal_item').modal('show');
}

function excluirItem(indice) {
    $('#modal_confirmar').modal('hide');
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registroItens.length - 1) {
        registroItens.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registroItens.shift();
    } else {
        let auxInicio = registroItens.slice(0, indice);
        let auxFim = registroItens.slice(indice + 1);
        registroItens = auxInicio.concat(auxFim);
    }
    preencheItens();
}

function escolherProposta(indice) {

    document.getElementById('finalizarCotacao').disabled = true;
    $('#btn'+indice + '.selecao').each(function( index ) {
            this.className = 'btn btn-success btn-xs selecao';
            this.innerHTML = 'Proposta Escolhida';
            document.getElementById('finalizarCotacao').disabled = false;
    });

    $('.valor-orcamento').each(function( index ) {
        //console.log( index + ": " + $( this ).text() );                
        //console.log('btn', '#btn'+indice);
        //console.log('data-valor', $( this ).data('saldo'));
        //console.log('ValorTotal', registros[indice].ValorTotal );
        let saldo = $( this ).data('saldo') - registros[indice].ValorTotal;
        $( this ).removeClass('text-danger');
        if (saldo < 0) {
            $( this ).addClass('text-danger');
        }
        $( this ).data('saldo', saldo);
        $( this ).html(formatValor(saldo, 'R$ '));
    });

    toastr.options.preventDuplicates = true;
    toastr.options.positionClass = "toast-top-center";
    toastr.success(`Proposta escolhida com sucesso!`);
    toastr.options.timeOut = 5000;
}

function carregaEmpreendimento() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/obrasTecnica');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selEmpreendimento').innerHTML = '';
            document.getElementById('selFiltroEmpreendimento').innerHTML = '';
            $('#selEmpreendimento').append(`<option value=""></option>`);
            $('#selFiltroEmpreendimento').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selEmpreendimento').append(`<option value="${data[x].CodigoObra}">${data[x].Nome}</option>`);
                $('#selFiltroEmpreendimento').append(`<option value="${data[x].CodigoObra}">${data[x].Nome}</option>`);


            }
        }
    }
    xhr.send();
}

function carregaSubMaterial() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/Material');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            subMaterial = data;
            document.getElementById('selMaterialServicoSub').innerHTML = '';
            $('#selMaterialServicoSub').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selMaterialServicoSub').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);

            }
        }
    }
    xhr.send();
}

function carregaCondicaoPagamento() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/condicaoPagamento');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;            
            document.getElementById('selCondicaoPagamento').innerHTML = '';             
            $('#selCondicaoPagamento').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selCondicaoPagamento').append(`<option value="${data[x].Codigo}">${data[x].Condicao}</option>`);

            }
        }
    }
    xhr.send();
}

function modalItem() {
    document.getElementById('form_item').reset();
    document.getElementById('mensagem_item').hidden = true;
    $('#modal_item').modal('show');
}

function carregaMaterial() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/material');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selFiltroMaterialServico').innerHTML = '';
            document.getElementById('selFiltroMaterialServico').innerHTML = '';
            $('#selFiltroMaterialServico').append(`<option value=""></option>`);
            $('#selMaterialServico').append(`<option value=""></option>`);
            for (let x in data) {
                $('#selFiltroMaterialServico').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);
                $('#selMaterialServico').append(`<option value="${data[x].CodigoMaterial}">${data[x].Descricao}</option>`);
            }
        }
    }
    xhr.send();
}

function carregaFornecedor() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/fornecedor');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selFornecedor').innerHTML = '';
            $('#selFornecedor').append(`<option value=""></option>`);
            //    $('#selFiltroFornecedor').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selFornecedor').append(`<option value="${data[x].CodigoFornecedor}">${data[x].Nome}</option>`);

            }
        }
    }
    xhr.send();
}
//Função para comparação de empreendimento material 
function comparaEmpreendimentoMaterial(empreendimento, material) {
    if (empreendimento != '' | material != '') {
        console.log(empreendimento, material);
        let xhr = new XMLHttpRequest();
        xhr.open('GET', BASE_URL_SERVICO + '/propostaOrcada', false);
        xhr.send();
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registroItensProposta = data;
            
            let tabela = document.getElementById('lista_corpo_proposta');
            tabela.innerHTML = '';
            let hasValorOrcamento = false;
            for (let i in registroItensProposta) {
                if (empreendimento == registroItensProposta[i].CodigoObra && material == registroItensProposta[i].CodigoMaterial) {
                    hasValorOrcamento = true;
                    tabela.innerHTML +=
                        `
                    <tr>
                                            
                    <td>${registroItensProposta[i].DescricaoEmprendimento}</td>
                    <td>${registroItensProposta[i].DescricaoMaterial}</td>
                    <td class="valor-orcamento" data-valor="${registroItensProposta[i].ValorMaterial}" data-saldo="${registroItensProposta[i].ValorMaterial}">${formatValor(registroItensProposta[i].ValorMaterial, 'R$ ')}</td>
                    </tr>`

                }
            }
            if (!hasValorOrcamento) {
                toastr.options.preventDuplicates = true;
                toastr.options.positionClass = "toast-top-center";
                toastr.error('Não existe valor de orçamento para sua pesquisa');
                toastr.options.timeOut = 5000;            
            }
        }
    }
};

function carregaPropostas(empreendimento, material, valor) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/propostaTecnica/' + empreendimento + '/' + material + '/' + valor);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registros = data;
            if (registros.length == 0) {
                toastr.options.preventDuplicates = true;
                toastr.options.positionClass = "toast-top-center";
                toastr.error('Não existem cotações para sua pesquisa');
                toastr.options.timeOut = 5000;
            }
            preencheTable();
            ViewTablePreco();
            ViewTableAnalitico();
        }
    }
    xhr.send();
}

function listarRegistros() {
    document.getElementById('lblFornecedorMenorPrecoMaterial').innerHTML = '';
    document.getElementById('lblFornecedorMenorPrecoMaoDeObra').innerHTML = '';
    document.getElementById('lblFornecedorMenorPrecoTotal').innerHTML = '';
    let empreendimento = document.getElementById('selFiltroEmpreendimento').value;
    let material = document.getElementById('selFiltroMaterialServico').value;
    let valor = document.getElementById('selFiltroValor').value;
    document.getElementById('finalizarCotacao').disabled = true;
    carregaPropostas(empreendimento, material, valor);
    comparaEmpreendimentoMaterial(empreendimento, material);
    return false;
}

function finalizarCotacao() {
    let buttons = document.getElementsByClassName('selecao');
    let propostas = [];
    let validada = false;
    //3 VISÕES COM OS MESMOS BUTTONS => buttons.length/3. EVITA ADD REG DUPLIACADO.
    for (let i = 0; i < buttons.length/3; i++) {
        let btn = buttons[i].id.replace('btn', '');
        let codigoProposta = registros[btn].CodigoProposta;
        if (buttons[i].innerHTML == 'Proposta Escolhida') {
            let proposta = {};
            proposta.CodigoProposta = codigoProposta;
            proposta.Escolhida = 'S';
            proposta.Validada = 'S';
            propostas.push(proposta);
            validada = true;
        } else {
            let proposta = {};
            proposta.CodigoProposta = codigoProposta;
            proposta.Escolhida = 'N';
            proposta.Validada = 'S';
            propostas.push(proposta);
        }
    }

    if (validada) {
        toastr.options.preventDuplicates = true;
        toastr.options.positionClass = "toast-top-center";
        toastr.success('Proposta finalizada com sucesso!');
        toastr.options.timeOut = 5000;
    
        let dados = JSON.stringify(propostas);
        gravarBD(dados);
        listarRegistros();    
    }
}

//funcao para alterar um registro no bd
function alterarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/propostaTecnica");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/propostaTecnica/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}
//funcao para salvar um registro no bd
function gravarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/propostaTecnica/escolherProposta", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
    }
};

function ViewTablePreco() {
    $('#lista_preco_corpo').html('');
    let tbody = "";
    let subItens = GetSubItens();
    for (let i in registros) {
        tbody +=`   
            <td>
                <table id="table_preco_${i}" border="1" class="table table-bordered text-center print" style="white-space:nowrap; width:350px;">
                    <tr class="cotacao">
                        <td colspan="4" style="text-align: center;">
                            <strong>${registros[i].Fornecedor}</strong><br>
                            <small>${formataDataBrasileira(registros[i].Data)}</small>
                        </td>
                        </tr>
                            <td>
                                <strong>Unidade</strong>
                            </td>
                            <td>
                                <strong>Quantidade</strong>
                            </td>
                            <td>
                                <strong>Valor Unitário</strong>
                            </td>
                            <td>
                                <strong>Valor Total</strong>
                            </td>
                        </tr> 
                    </tr>`;
        for (let s in subItens) {                                                
            let item =  registros[i].Itens.filter(x => x.CodigoMaterialSub === subItens[s].CodigoMaterialSub);
            if (item && item.length !== 0) {  
                tbody += `                    
                <tr style="background-color: #f2f2f2;">
                    <td colspan="4" style="text-align: center;">
                        <strong style="color:#337ab7;">${item[0].Descricao}</strong><br>
                        <samll>${item[0].DescricaoComplementar}&nbsp;</small>
                    </td>
                </tr>  
                <tr>
                    <td>
                        ${item[0].UnidadeMedida}
                    </td>
                    <td>
                        ${item[0].Quantidade}
                    </td>
                    <td>
                        ${formatValor(item[0].ValorUnitario, 'R$ ')}
                    </td>
                    <td>                            
                        ${formatValor(item[0].ValorUnitario * item[0].Quantidade, 'R$ ')}
                    </td>
                </tr>                 
                `;
            }
            else{
                tbody += `                                    
                <tr style="background-color: #f2f2f2;">
                    <td colspan="4">
                        <strong style="color: #0000cc;">&nbsp;</strong><br>
                        <samll>&nbsp;</small>                        
                    </td>                        
                </tr>                 
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>                        
                </tr>                 
                `;
            }
        }
        tbody +=`
                    <tr rowspan="3" >
                        <td colspan="4" class="text-left">
                            &nbsp;
                        </td>
                    </tr>                    
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="4" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    <strong>Mão de Obra: </strong>
                                </div>
                                <div class="col col-sm-6 text-right">
                                    ${formatValor(registros[i].ValorMaoDeObra, 'R$ ')}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="4" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    <strong>Frete: </strong>
                                </div>
                                <div class="col col-sm-6 text-right">
                                    ${formatValor(registros[i].ValorFrete, 'R$ ')}        
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="4" class="text-left">
                            <div class="row">
                                <div class="col col-sm-4">
                                    <strong>Condição de Pagamento: </strong>
                                </div>
                                <div class="col col-sm-4 text-right">
                                    ${registros[i].DescCondPag}
                                </div>
                            </div>                            
                        </td>
                    </tr>
                    <tr style="background-color: silver">
                        <td colspan="4" class="text-left">
                            <div class="row">
                                <div class="col col-sm-4">
                                    <strong>Valor Total: </strong>
                                </div>
                                <div class="col col-sm-4 text-right">
                                    <h4>${formatValor(registros[i].ValorTotal, 'R$ ')}</h4>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="white-space: nowrap">
                            <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                            <button class="btn btn-danger btn-xs selecao" title="Escolher Proposta" id="btn${i}" onclick="escolherProposta(${i})">Escolher Proposta</button>&nbsp;
                            <button class="btn btn-dark btn-xs glyphicon glyphicon-print" title="Imprimir" onclick="printData('table_preco_${i}')"></button>
                        </td>
                    </tr>
                </table>
            </td>
        `;
    }
    $('#lista_preco_corpo').html(tbody);
}

function ViewTableAnalitico() {
    $('#lista_analitico_corpo').html('');    
    let tbody = "";    
    let subItens = GetSubItens();
    for (let i in registros) {
        tbody +=`   
            <td>
                <table id="table_analitico_${i}"  border="1"  class="table table-bordered text-center print" style="white-space:nowrap; width:350px;">
                    <tr class="cotacao">
                        <td colspan="5" style="text-align: center;">
                            <strong>${registros[i].Fornecedor}</strong><br>
                            <small>${formataDataBrasileira(registros[i].Data)}</small>
                        </td>
                        </tr>
                            <td>
                                <strong>Unidade</strong>
                            </td>
                            <td>
                                <strong>Quantidade</strong>
                            </td>
                            <td>
                                <strong>Unidade/m2</strong>
                            </td>
                            <td>
                                <strong>Material (R$/Unidade)</strong>
                            </td>
                            <td>
                                <strong>Valor Total</strong>
                            </td>
                        </tr> 
                    </tr>`;
        for (let s in subItens) {                                                
            let item =  registros[i].Itens.filter(x => x.CodigoMaterialSub === subItens[s].CodigoMaterialSub);
            if (item && item.length !== 0) {                  
                tbody += `                    
                <tr style="background-color: #f2f2f2;">
                    <td colspan="5" style="text-align: center;">
                        <strong style="color:#337ab7;">${item[0].Descricao}</strong><br>
                        <samll>${item[0].DescricaoComplementar}&nbsp;</small>
                    </td>
                </tr>  
                <tr>
                    <td>
                        ${item[0].UnidadeMedida}
                    </td>
                    <td>
                        ${item[0].Quantidade}
                    </td>
                    <td>
                        ${(item[0].Quantidade / registros[i].Area).toFixed(2)}
                    </td>
                    <td>
                        ${formatValor(item[0].ValorMaterial / registros[i].Quantidade, 'R$ ')}
                    </td>
                    <td>                            
                        ${formatValor(item[0].ValorUnitario * item[0].Quantidade, 'R$ ')}
                    </td>
                </tr>                 
                `;
            }
            else{
                tbody += `                                    
                <tr style="background-color: #f2f2f2;">
                    <td colspan="5">
                        <strong style="color: #0000cc;">&nbsp;</strong><br>
                        <samll>&nbsp;</small>                        
                    </td>                        
                </tr>                 
                <tr>
                    <td colspan="5">
                        &nbsp;
                    </td>                        
                </tr>                 
                `;
            }
        }
        tbody +=`
                    <tr rowspan="3" >
                        <td colspan="5" class="text-left">
                            &nbsp;
                        </td>
                    </tr> 
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="5" class="text-left">
                            <strong>Indicadores (R$/${registros[0].Itens[0].UnidadeMedida})</strong>
                        </td>
                    </tr>                    
                    <tr>
                        <td colspan="5" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    Material:
                                </div>
                                <div class="col col-sm-6 text-right">
                                   ${ (registros[i].ValorMaterial  / registros[i].Quantidade).toFixed(1) }
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    Mão de Obra:
                                </div>
                                <div class="col col-sm-6 text-right">
                                    ${ (registros[i].ValorMaoDeObra / registros[i].Quantidade).toFixed(1) }
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-left">
                            <div class="row">
                                <div class="col col-sm-6">
                                    Combinado:
                                </div>
                                <div class="col col-sm-6 text-right">                                    
                                    ${ ((registros[i].ValorMaterial + registros[i].ValorMaoDeObra) / registros[i].Quantidade).toFixed(1) }
                                </div>
                            </div>                            
                        </td>
                    </tr>                    

                    <tr rowspan="3" >
                        <td colspan="5" class="text-left">
                            &nbsp;
                        </td>
                    </tr>                    
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="5" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    <strong>Mão de Obra: </strong>
                                </div>
                                <div class="col col-sm-6 text-right">
                                    ${formatValor(registros[i].ValorMaoDeObra, 'R$ ')}
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="5" class="text-left">
                            <div class="row">    
                                <div class="col col-sm-6">
                                    <strong>Frete: </strong>
                                </div>
                                <div class="col col-sm-6 text-right">
                                    ${formatValor(registros[i].ValorFrete, 'R$ ')}        
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #f2f2f2;">
                        <td colspan="5" class="text-left">
                            <div class="row">
                                <div class="col col-sm-4">
                                    <strong>Condição de Pagamento: </strong>
                                </div>
                                <div class="col col-sm-4 text-right">
                                    ${registros[i].DescCondPag}
                                </div>
                            </div>                            
                        </td>
                    </tr>
                    <tr style="background-color: silver">
                        <td colspan="5" class="text-left">
                            <div class="row">
                                <div class="col col-sm-4">
                                    <strong>Valor Total: </strong>
                                </div>
                                <div class="col col-sm-4 text-right">
                                    <h4>${formatValor(registros[i].ValorTotal, 'R$ ')}</h4>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="white-space: nowrap">
                            <button class="btn btn-primary btn-xs glyphicon glyphicon-edit" title="Editar" onclick="editarRegistro(${i})"></button>&nbsp;
                            <button class="btn btn-danger btn-xs selecao" title="Escolher Proposta" id="btn${i}" onclick="escolherProposta(${i})">Escolher Proposta</button>&nbsp;
                            <button class="btn btn-dark btn-xs glyphicon glyphicon-print" title="Imprimir" onclick="printData('table_analitico_${i}')"></button>
                        </td>
                    </tr>
                </table>
            </td>
        `;
    }
    $('#lista_analitico_corpo').html(tbody);
}

function GetSubItens() {   
    let subItens = [];
    registros.forEach(reg => {
       reg.Itens.forEach(it => {
            let index =  subItens.filter(x => x.CodigoMaterialSub === it.CodigoMaterialSub);
            if (index && index.length === 0) {
                subItens.push(it);
            }
       });
    });
    return subItens;
}

function View(type){    
    $('#lista_registros').hide();
    $('#lista_preco').hide();
    $('#lista_analitico').hide();
    switch (type) {
        case 0:
            $('#lista_registros').show();
            break;
        case 1:
            $('#lista_preco').show();                  
            break;
        case 2:
            $('#lista_analitico').show();            
            break;
        default:
            $('#lista_registros').show();
            break;
    }
}

function printData(id) {      
   let html = `
        <strong>Empreendimento: </strong> ${$('#selFiltroEmpreendimento option:selected').text()}
        <strong>Material ou Serviço: </strong> ${$('#selFiltroMaterialServico option:selected').text()}

        <br>
        <br>

        ${$('#'+id)[0].outerHTML}
   `;
   newWin= window.open("");
   newWin.document.write(html);
   newWin.print();
   newWin.close();
}