var registros = [];
let codigo;
let pergunta;
let resposta;
let frequencia;
let abaQuestionario1 = 1;
let abaQuestionario2 = 2;
let abaQuestionario3 = 3;
let abaQuestionario4 = 4;
let abaQuestionario5 = 5;
let abaQuestionario6 = 6;
let abaQuestionario7 = 7;
let abaQuestionario8 = 8;
let abaQuestionario9 = 9;
let abaQuestionario10 = 10;
let abaQuestionario11 = 11;




$(window).on("load", function () {
    carregaAvaliado();
});


function buscaresposta(qtddelinhas, abaQuestionario) {
    var contador = 1;
    for (let p = 1; p < qtddelinhas; p++) {

        let rates = document.getElementsByTagName("input");

        if (rates[p].checked) {

            frequencia = rates[p].value;
            resposta = p;
            pergunta = contador++;
            recebeaba1(pergunta, resposta, frequencia, abaQuestionario);
            console.log('FREQUÊNCIA', frequencia, 'RESPOSTA', resposta, 'PERGUNTA', pergunta);

        }

    }
}

function recebeaba1(pergunta, resposta, frequencia, abaQuestionario) {
    debugger
    let codigo = document.getElementById('txtCodigo').value;
    let codgiAvaliado = document.getElementById('selCodigoAvaliado').value;
    registro = {};
    if (codigo != '') {
        registro.CodigoAvaliacaoComportamento = codigo;
    } else {
        //captura o codigo do usuario
        var cookie = $.parseJSON($.cookie('VP6_Patrimar_Session'));
        registro.InseridoPor = cookie.CodigoUsuario;
    }

    registro.CodigoAvaliacaoComportamento = codigo;
    registro.Tipo = abaQuestionario;
    registro.Pergunta = pergunta;
    registro.Resposta = resposta;
    registro.Frequencia = frequencia;
    registro.Cod_Avaliado = codgiAvaliado;
    console.log(registro);
    let dados = JSON.stringify(registro);
    gravarBD(dados);
}

function receberQuestaoAberta(abaQuestionario, questaoAberta, pergunta, frequencia) {
    debugger
    let codigo = document.getElementById('txtCodigo').value;
    let codgiAvaliado = document.getElementById('selCodigoAvaliado').value;
    registro = {};
    if (codigo != '') {
        registro.CodigoAvaliacaoComportamento = codigo;
    } else {
        //captura o codigo do usuario
        var cookie = $.parseJSON($.cookie('VP6_Patrimar_Session'));
        registro.InseridoPor = cookie.CodigoUsuario;
    }
    registro.Tipo = abaQuestionario;
    registro.Pergunta = pergunta;
    registro.Resposta = questaoAberta;
    registro.Frequencia = frequencia;
    registro.Cod_Avaliado = codgiAvaliado;
    console.log(registro);
    let dados = JSON.stringify(registro);
    gravarBD(dados);
}


function salvarPerguntas() {
    debugger
    verificaQuestao11();
    //Questão 1
    if (abaQuestionario1 == 1) {
        buscaresposta(12, abaQuestionario1);

    }
    //Questão 2
    if (abaQuestionario2 == 2) {
        buscaresposta(8, abaQuestionario2);
    }

    //Questão 3
    if (abaQuestionario3 == 3) {
        buscaresposta(8, abaQuestionario3);
    }
    //Questão 4
    if (abaQuestionario4 == 4) {
        buscaresposta(8, abaQuestionario4);
    }
    //Questão 5
    if (abaQuestionario5 == 5) {
        buscaresposta(8, abaQuestionario5);
    }
    //Questão 6
    if (abaQuestionario6 == 6) {
        buscaresposta(8, abaQuestionario6);

    }
    //Questão 7
    if (abaQuestionario7 == 7) {
        buscaresposta(8, abaQuestionario7);

    }
    //Questão 8
    if (abaQuestionario8 == 8) {
        buscaresposta(32, abaQuestionario8);

    }
    //Questão 9
    if (abaQuestionario9 == 9) {
        buscaresposta(8, abaQuestionario9);
    }
    //Questão 10
    if (abaQuestionario10 == 10) {
        buscaresposta(8, abaQuestionario10);


    }
    //Questão 11
    if (abaQuestionario11 == 11) {

        var questao1 = document.getElementById('txtArea1').value;
        var questao2 = document.getElementById('txtArea2').value;
        var questao3 = document.getElementById('txtArea3').value;

        if (questao1 != '' && questao2 != '' && questao3 != '') {
            receberQuestaoAberta(abaQuestionario11, questao1, 1, 0);

            receberQuestaoAberta(abaQuestionario11, questao2, 2, 0);

            receberQuestaoAberta(abaQuestionario11, questao2, 3, 0);
        }

    }
}

function verificaQuestao11() {

    var questao1 = document.getElementById('txtArea1').value;
    var questao2 = document.getElementById('txtArea2').value;
    var questao3 = document.getElementById('txtArea3').value;

    if ((undefined) || (questao1 == '' && questao2 == '' && questao3 == '')) {
        debugger
        alert("Questão aberta não preenchida!");

    }
    /* else {
           exbirmensagemSalvar();
           
       } */
}

//Pop-up para não perder os dados da pesquisa
function exibirConfirmarSemSalvar() {
    var mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Deseja mesmo sair da avaliação seus dados marcados serão exluídos?';
    //captura o button de confirmar do modal_confirmar
    var btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    var onClick = document.createAttribute('onclick');
    //define o evento onclick
    onClick.value = 'redireciona()';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    $('#modal_confirmar').modal('show');


}
//Redireciona a página 
function redireciona() {
    location.href = "principal.php";

}

function exbirmensagemSalvar() {

    $('#modal_abertas').modal('show');
}



//funcao para gravar um novo registro no bd
function gravarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/avaliacaoQualidade", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
        return data[0].id;
    }
};

//funcao para alterar um registro no bd
function alterarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/avaliacaoQualidade", false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/avaliacaoQualidade/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}

$(document).on("keydown", "#txtArea1", function () {
    var caracteresRestantes = 255;
    var caracteresDigitados = parseInt($(this).val().length);
    var caracteresRestantes = caracteresRestantes - caracteresDigitados;

    $(".caracteres").text(caracteresRestantes);
});

$(document).on("keydown", "#txtArea2", function () {
    var caracteresRestantes = 255;
    var caracteresDigitados = parseInt($(this).val().length);
    var caracteresRestantes = caracteresRestantes - caracteresDigitados;

    $(".caracteres1").text(caracteresRestantes);
});
$(document).on("keydown", "#txtArea3", function () {
    var caracteresRestantes = 255;
    var caracteresDigitados = parseInt($(this).val().length);
    var caracteresRestantes = caracteresRestantes - caracteresDigitados;

    $(".caracteres2").text(caracteresRestantes);
});

//funcao para carregar os estados
function carregaAvaliado() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", BASE_URL_SERVICO + "/usuario");
    xhr.onload = function () {
        if (xhr.status === 200) {
            var data = $.parseJSON(xhr.responseText).result;

            for (i = 0; i < data.length; i++) {
                $('#selCodigoAvaliado').append(
                    '<option value="' + data[i].CodigoUsuario + '">' + data[i].Nome + '</option>'
                );
            }
        }
    }
    xhr.send();
};