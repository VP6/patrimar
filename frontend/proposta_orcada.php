<?php
    require_once("layout/cabecalho_layout.php");
?>

    <div class="container">

        <!-- formulario filtro -->
        <div class="panel panel-primary" id="filtros">
            <div class="panel-heading">Filtros</div>
            <div class="panel-body">
                <form class="form-inline">
                    <label class="control-label" for="txtBuscar">Pesquise na tabela por:</label>
                    <input type="text" id="txtBuscar" placeholder="Buscar por" class="form-control input-sm">&nbsp;                    
                </form>
            </div>
            <div class="panel-footer" >   
                <button type="button" class="btn btn-success btn-sm" onclick="novoRegistro()">Inserir</button>                    
                <button type="button" class="btn btn-primary btn-sm" onclick="AtualizarRegistros()" title="Selecione os registros para atualizar os valores">Atualizar Orçamento</button>
                <button type="button" class="btn btn-secondary btn-sm glyphicon glyphicon-refresh" onclick="Refresh()" title="Refresh"></button>
            </div>
        </div>

        <!-- tabela dos registros -->
        <div class="panel panel-primary" id="lista">
            <div class="panel-heading">Valores de orçamento</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="lista_registros">
                        <thead>
                            <tr>
                                <th>
                                    <input class="form-check-input" type="checkbox" id="ckbSelecionar" name="ckbSelecionar" onchange="checkAll(this)">
                                </th>
                                <th>Empreendimento</th>
                                <th>Material ou serviço</th>
                                <th>Valor</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="lista_corpo">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Formulario para manipulacao dos registros -->
        <form onsubmit="salvarRegistro()" id="formulario" method="post" hidden>
            <div class="panel panel-primary">
                <div class="panel-heading">Valor de orçamento</div>
                <div class="panel-body">
                    <div class="form-group row">
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <input type="text" name="txtCodigo" id="txtCodigo" hidden>
                            <label class="control-label" for="selEmpreendimento">Empreendimento</label>
                            <select id="selEmpreendimento" class="form-control" required>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-4">
                            <label class="control-label" for="selMaterialServico">Material ou serviço</label>
                            <select id="selMaterialServico" class="form-control" required>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group-sm col-sm-12 col-md-4">
                                <label class="control-label" for="txtValorMaterial">Valor:</label>
                                <input type="text" id="txtValorMaterial" class="form-control valor" placeholder="Valor material" required>
                            </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    <button type="button" class="btn btn-default btn-sm" onclick="cancelarRegistro()">Cancelar</button>
                </div>
            </div>
        </form>

        <!-- Formulario modal de confirmacao-->
        <div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Confirmar</h5>
                    </div>
                    <div class="modal-body">
                        <p id="mensagem_modal_confirmar"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btnModalConfirmar">Sim</button>
                        <button type="button" class="btn btn-danger" id="btnModalCancelar" data-dismiss="modal">Não</button>
                        <button type="button" class="btn btn-success" id="btnModalOk" data-dismiss="modal" hidden>Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Formulario modal de Atualizar-->
        <div class="modal fade" id="modal_atualizar" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Confirmar</h5>
                    </div>
                    <div class="modal-body">                                                                                                                                            
                        <form class="form-inline">
                            <label class="control-label" for="txtValorAtualizacao">Defina o índice para reajustar os valores dos itens abaixo: </label>
                            <input type="number" id="txtValorAtualizacao" class="form-control" placeholder="0" required>                            
                        </form>    
                        <br >                                            
                        <table id="tbOrcamentosAtualizacao" class="table"></table>                        
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-success" id="btnModalConfirmarAlteracao" onClick="AtualizarValor();">Sim</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="js/mask.js"></script>
    <script src="js/mask_money.js"></script>
    <script src="js/config.js"></script>
    <script src="js/formularios/proposta_orcada.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <?php
    require_once("layout/rodape_layout.php");
?>